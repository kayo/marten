#include <marten.h>
#include <uv.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#define LOG_LEVEL LOG_DEBUG

#include "debug.h"

#define N_CONNECTIONS (1<<10)
#define BUFFER_LENGTH 128 // (1<<10) //(64<<10)
#define TERMINAL_SIZE (4<<10)

typedef struct {
  size_t cnt; /* number of last written character */
  char *ptr;  /* pointer to last position */
  size_t len; /* length of data in buffer */
  char buf[TERMINAL_SIZE]; /* data buffer */
} terminal_t;

static void terminal_push(terminal_t *term, const char *ptr, size_t len) {
  if (len == 0) {
    return;
  }
  
  if (len > sizeof(term->buf)) {
    len = sizeof(term->buf);
  }

  char *term_end = term->buf + sizeof(term->buf);
  size_t wbs;

 start:
  wbs = term_end - term->ptr;

  if (wbs > len) {
    wbs = len;
  }
  
  memcpy(term->ptr, ptr, wbs);
  
  term->cnt += wbs;
  term->ptr += wbs;
  
  if (term->len < sizeof(term->buf)) {
    term->len += wbs;
  }
  
  ptr += wbs;
  len -= wbs;
  
  if (term->ptr == term_end) {
    term->ptr = term->buf;
  }

  if (len > 0) {
    goto start;
  }
}

static int terminal_pull(terminal_t *term, size_t pos, const char **ptr, size_t *len) {
  if (pos >= term->len) {
    *len = 0;
    
    return 0;
  }
  
  if (pos == (size_t)-1 || pos < term->cnt - term->len) {
    pos = term->cnt - term->len;
  }
  
  *len = term->cnt - pos;
  
  if (*len > (size_t)(term->ptr - term->buf)) {
    *len -= term->ptr - term->buf;
    
    char *end = term->buf + sizeof(term->buf);
    
    *ptr = end - *len;
    return 1;
  }
  
  *ptr = term->ptr - *len;
  return 0;
}

typedef struct {
  const marten_pool_t *pool;
  uv_tcp_t handle;
  terminal_t terminal;
} server_t;

typedef struct {
  size_t from;
} get_terminal_t;

typedef struct {
  size_t size;
} put_terminal_t;

typedef struct {
  marten_conn_state_t state;
  marten_request_parser_t request;
  union {
    marten_query_parser_t query;
    marten_head_parser_t header;
  };
  marten_route_t route;
  marten_hash_t hash, key_hash;
  marten_response_writer_t response;
  
  uv_tcp_t handle;
  uv_write_t write_req;
  server_t *server;
  
  union {
    get_terminal_t get_terminal;
    put_terminal_t put_terminal;
  };
  
  char buffer[BUFFER_LENGTH];
} sconn_t;

static const char *ent_name(marten_flags_t flags) {
  switch (flags & marten_entity) {
  case marten_method: return "method";
  case marten_path: return "path";
  case marten_query: return "query";
  case marten_proto: return "proto";
  case marten_head: return "head";
  case marten_body: return "body";
  case marten_content: return "content";
  }
  return "unknown";
}

static const char *ent_part(marten_flags_t flags) {
  switch (flags & marten_entry) {
  case marten_field: return " field";
  case marten_value: return " value";
  }
  return "";
}

#define flags_fmt "[%s%s%s%s]"

#define show_flags(flags)                        \
  (flags & marten_init ? "init " : ""),          \
    (flags & marten_done ? "done " : ""),        \
    ent_name(flags), ent_part(flags)

extern const uint8_t index_html_gz[];
extern const uint32_t index_html_gz_size;

static marten_define_writer(get_root_response) {
  sconn_t *conn = marten_containerof(writer, sconn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "text/html");
  marten_header_cstr_write(writer, "Content-Encoding", "gzip");
  {
    char *ptr = conn->buffer;
    ptr = marten_cstr_tostr(ptr, "Content-Length: ");
    ptr = marten_uint_tostr(ptr, index_html_gz_size);
    ptr = marten_cstr_tostr(ptr, marten_feed);
    marten_raw_write(writer, conn->buffer, ptr - conn->buffer);
  }
  marten_feed_write(writer);
  marten_raw_write(writer, index_html_gz, index_html_gz_size);
  marten_end(writer);
}

static marten_define_handler(get_root_handler) {
  /* because we use the same handler to handle request
     and query parameters we need correctly get container */
  sconn_t *conn = marten_is_ent(flags, marten_query)
    ? marten_containerof(state, sconn_t, query)
    : marten_containerof(state, sconn_t, route);
  
  if (marten_is_raw(flags, marten_query)) {
    /* you can parse query variables using same handler */
    marten_query_parse(&conn->query, get_root_handler, flags, ptr, len);
  }
  
  if (marten_has(flags, marten_done)) { /* entity is done */
    if (marten_has(flags, marten_value)) { /* entity has value */
      if (marten_is(flags, marten_proto)) { /* entity is request protocol */
        /* now you can simply compare hashes instead of comparing string */
        if (conn->key_hash == marten_hash("HTTP")) {
          debug("Proto is HTTP");
        } else {
          debug("Proto isnt HTTP");
        }
        if (conn->hash == marten_hash("1.1")) {
          debug("Proto version is 1.1");
        } else {
          debug("Proto version isnt 1.1");
        }
      }
    }
    
    if (marten_is(flags, marten_head) &&
        !marten_has(flags, marten_field | marten_value)) {
      debug("Headers is done");
      
      marten_respond(&conn->response, get_root_response);
    }
  }
  
  return 0;
}

static marten_define_writer(get_pong_response) {
  sconn_t *conn = marten_containerof(writer, sconn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "text/plain");
  marten_header_cstr_write(writer, "Content-Length", "4");
  marten_feed_write(writer);
  marten_cstr_write(writer, "pong");
  marten_end(writer);
}

static marten_define_handler(get_ping_handler) {
  (void)flags;
  (void)ptr;
  (void)len;
  
  sconn_t *conn = marten_containerof(state, sconn_t, route);
  
  marten_respond(&conn->response, get_pong_response);
  
  return 1;
}

static marten_define_writer(get_terminal_notifier) {
  sconn_t *conn = marten_containerof(writer, sconn_t, response);
  
  debug("conn %p: terminal notify last-event-id=%zu", conn, conn->get_terminal.from);
  
  marten_begin(writer);
  marten_set(conn->state, marten_conn_send);
  
  for (; conn->get_terminal.from < conn->server->terminal.cnt; ) {
    marten_cstr_write(writer, "data: ");
    
    const char *ptr = NULL;
    size_t len;
    
    terminal_pull(&conn->server->terminal, conn->get_terminal.from, &ptr, &len);
    
    if (len > 0) {
      const char *end = ptr + len;
      const char *chr = ptr;

      for (; chr < end; chr++)
        if (*chr == '\n') break;

      len = chr - ptr;
      
      if (len > 0) {
        conn->get_terminal.from += len;
        marten_raw_write(writer, ptr, len);
        
        ptr = NULL;
        terminal_pull(&conn->server->terminal, conn->get_terminal.from, &ptr, &len);
      }
      
      char *str = conn->buffer;

      if (*ptr == '\n') {
        conn->get_terminal.from++;
        str = marten_cstr_tostr(str, "\ndata: ");
      }
      
      str = marten_cstr_tostr(str, "\nid: ");
      str = marten_uint_tostr(str, conn->get_terminal.from);
      
      str = marten_cstr_tostr(str, "\n\n");

      marten_raw_write(writer, conn->buffer, str - conn->buffer);
    }
  }
  
  marten_reset(conn->state, marten_conn_send);
  marten_end(writer);
}

static marten_define_writer(get_terminal_response) {
  sconn_t *conn = marten_containerof(writer, sconn_t, response);
  
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Cache-Control", "no-cache");
  marten_header_cstr_write(writer, "Content-Type", "text/event-stream");
  marten_feed_write(writer);
  marten_end(writer);
  
  marten_set(conn->state, marten_conn_data);
  marten_respond(&conn->response, get_terminal_notifier);
}

static marten_define_handler(get_terminal_handler) {
  /* because we use the same handler to handle request
     and query parameters we need correctly get container */
  sconn_t *conn = marten_containerof(state, sconn_t, route);
  
  if (marten_is(flags, marten_head) &&
      marten_has(flags, marten_value) &&
      conn->key_hash == marten_hash("last-event-id")) {
    /* read last position */
    marten_uint_parse(conn->get_terminal.from, flags, ptr, len);
    if (marten_has(flags, marten_done) &&
        conn->get_terminal.from > conn->server->terminal.cnt - conn->server->terminal.len) {
      /* resend all data */
      conn->get_terminal.from = conn->server->terminal.cnt - conn->server->terminal.len;
    }
    debug("set last-event-id=%lu", conn->get_terminal.from);
  }

  if (marten_is(flags, marten_proto) &&
      marten_has(flags, marten_value) &&
      marten_has(flags, marten_done)) {
    conn->get_terminal.from = conn->server->terminal.cnt - conn->server->terminal.len;
    debug("init last-event-id=%lu", conn->get_terminal.from);
  }
  
  if (marten_is(flags, marten_head) &&
      !marten_has(flags, marten_field | marten_value)) {
    if (marten_has(flags, marten_done)) {
      marten_respond(&conn->response, get_terminal_response);
      
      return 1;
    }
  }
  
  return 0;
}

static marten_define_writer(put_terminal_response) {
  sconn_t *conn = marten_containerof(writer, sconn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 204, "No Content");
  marten_conn_header_write(writer, conn->state);
  marten_feed_write(writer);
  marten_end(writer);
  
  const marten_pool_t *pool = conn->server->pool;

  /* notify all terminal listeners */
  for (conn = NULL; (conn = marten_conn_next(pool, conn)) != NULL; ) {
    if (marten_has(conn->state, marten_conn_data) &&
        !marten_has(conn->state, marten_conn_send)) {
      marten_respond(&conn->response, get_terminal_notifier);
    }
  }
}

static marten_define_handler(put_terminal_handler) {
  sconn_t *conn = marten_containerof(state, sconn_t, route);
  
  if (marten_is(flags, marten_head) &&
      marten_has(flags, marten_value)) {
    if (conn->key_hash == marten_hash("content-length")) {
      /* read size of content */
      marten_uint_parse(conn->put_terminal.size, flags, ptr, len);
      
      if (marten_has(flags, marten_done) &&
          conn->put_terminal.size == 0) {
        /* complete processing */
        marten_respond(&conn->response, put_terminal_response);
        
        return 1;
      }
    }
  }
  
  if (marten_is(flags, marten_body)) {
    if (len > conn->put_terminal.size) {
      len = conn->put_terminal.size;
    }
    
    terminal_push(&conn->server->terminal, ptr, len);
    conn->put_terminal.size -= len;

    if (conn->put_terminal.size == 0) {
      /* complete processing */
      marten_respond(&conn->response, put_terminal_response);
      
      return 1;
    }
  }

  /* continue processing */
  return 0;
}

marten_define_router(example_router,
                     ("GET", "/", get_root_handler),
                     ("GET", "/ping", get_ping_handler),
                     ("PUT", "/terminal", put_terminal_handler),
                     ("GET", "/terminal/sse", get_terminal_handler)
                     );

static marten_define_writer(notfound_response) {
  sconn_t *conn = marten_containerof(writer, sconn_t, response);
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 404, "Not Found");
  marten_conn_header_write(writer, conn->state);
  marten_header_cstr_write(writer, "Content-Type", "text/plain");
  marten_header_cstr_write(writer, "Content-Length", "18");
  marten_feed_write(writer);
  marten_cstr_write(writer, "Resource Not Found");
  marten_end(writer);
}

static marten_define_handler(on_conn_data) {
  sconn_t *conn = marten_is_ent(flags, marten_head) ?
    marten_containerof(state, sconn_t, header) :
    marten_containerof(state, sconn_t, request);
  
  rawlog("conn %p: parse " flags_fmt, ptr, len, conn, show_flags(flags));
  
  if (marten_is(flags, marten_method) &&
      marten_has(flags, marten_init)) {
    marten_reset(conn->state,
                 marten_conn_keep |
                 marten_conn_part |
                 marten_conn_data);
  }
  
  if (marten_is_raw(flags, marten_head)) {
    marten_head_parse(&conn->header, on_conn_data, flags, ptr, len);
  }
  
  /* you can pick fields and values by calculating its hashes */
  /* calculated hashes can be used to check values of fields */
  marten_field_pick(&conn->key_hash, flags, ptr, len);
  
  if ((marten_is(flags, marten_head) || marten_is(flags, marten_proto)) &&
      marten_has(flags, marten_value)) {
    if (conn->key_hash == marten_hash("HTTP") ||
        conn->key_hash == marten_hash("HTTPS") ||
        conn->key_hash == marten_hash("transfer-encoding") ||
        conn->key_hash == marten_hash("connection")) {
      marten_value_pick(&conn->hash, flags, ptr, len);
    } else if (conn->key_hash == marten_hash("keep-alive")) {
      /* ... */
    }

    if (marten_has(flags, marten_done)) {
      if (conn->key_hash == marten_hash("transfer-encoding")) {
        if (conn->hash == marten_hash("chunked")) {
          marten_set(conn->state, marten_conn_part);
        }
      } else if (conn->key_hash == marten_hash("connection")) {
        if (conn->hash == marten_hash("close")) {
          marten_reset(conn->state, marten_conn_keep);
        } else if (conn->hash == marten_hash("keep-alive")) {
          marten_set(conn->state, marten_conn_keep);
        }
      }
    }
  }
  
  int res = marten_router_handle(&example_router, &conn->route, &conn->hash, flags, ptr, len);
  
  if (res != 0) {
    if (conn->route == marten_route_none) {
      marten_respond(&conn->response, notfound_response);
    }
  }
  
  return res;
}

static void
on_conn_close(uv_handle_t* handle) {
  sconn_t *conn = marten_containerof(handle, sconn_t, handle);
  
  info("conn %p: close", conn);
  marten_conn_done(conn->server->pool, (marten_conn_t*)conn);
}

static void
alloc_buffer(uv_handle_t *handle,
             size_t suggested_size,
             uv_buf_t* buf) {
  (void)suggested_size;
  sconn_t *conn = marten_containerof(handle, sconn_t, handle);
  
  debug("conn %p: alloc_buffer() %lu bytes", conn, sizeof(conn->buffer));
  
  buf->base = conn->buffer;
  buf->len = sizeof(conn->buffer);
}

static void
on_conn_read(uv_stream_t *stream,
             ssize_t nread,
             const uv_buf_t *buf) {
  sconn_t *conn = marten_containerof(stream, sconn_t, handle);
  
  debug("conn %p: on_conn_read() nread=%zd", conn, nread);

  if (nread == UV_EOF) {
    info("conn %p: eof", conn);
    
    //debug("conn %p: uv_read_stop()", conn);
    //uv_read_stop((uv_stream_t*)&conn->handle);
    
    marten_request_parse(&conn->request, on_conn_data, marten_done, NULL, 0);
    marten_reset(conn->state, marten_conn_recv);
    
    //debug("conn %p: uv_read_start()", conn);
    //uv_read_start((uv_stream_t*)&conn->handle, alloc_buffer, on_conn_read);
    
    uv_close((uv_handle_t*)&conn->handle, on_conn_close);
    return;
  }
  
  if (nread < 0) {
    error("conn %p: read error", conn);
    
    uv_close((uv_handle_t*)&conn->handle, on_conn_close);
    return;
  }
  
  if (nread > 0) {
    rawlog("conn %p: recv", buf->base, nread, conn);
    
    if (marten_has(conn->state, marten_conn_recv)) {
      debug("conn %p: resume parse", conn);
      
      if (0 != marten_request_parse(&conn->request, on_conn_data, 0, buf->base, nread)) {
        marten_reset(conn->state, marten_conn_recv);
      }
    }
  }
}

static void
conn_reset(sconn_t *conn) {
  marten_set(conn->state, marten_conn_recv);
  
  marten_request_parse(&conn->request, on_conn_data, marten_init, NULL, 0);
}

static void
on_conn_sent (uv_write_t* req,
              int status) {
  sconn_t *conn = marten_containerof(req, sconn_t, write_req);
  
  if (status == 0) {
    if (0 == marten_resume_write(&conn->response)) {
      debug("conn %p: write resume %u", conn, conn->response.stage);
      return;
    }
  } else { /* close when error */
    uv_close((uv_handle_t*)&conn->handle, on_conn_close);
    return;
  }
  
  debug("conn %p: write done", conn);
  
  if (!marten_has(conn->state, marten_conn_keep)) {
    /* close connection */
    uv_close((uv_handle_t*)&conn->handle, on_conn_close);
  } else { /* restart parser */
    info("conn %p: keep", conn);
    
    conn_reset(conn);
  }
}

static marten_define_handler(conn_send) {
  (void)flags;
  
  sconn_t *conn = marten_containerof(state, sconn_t, response);
  
  const uv_buf_t buf = {
    .base = (void*)ptr,
    .len = len
  };
  
  rawlog("conn %p: send", ptr, len, conn);
  
  uv_write(&conn->write_req, (uv_stream_t*)&conn->handle, &buf, 1, on_conn_sent);
  
  return 0;
}

static void
on_conn_open(uv_stream_t *stream,
             int status) {
  server_t *server = marten_containerof(stream, server_t, handle);
  
  debug("on_conn_open()");
  
  if (status == -1) {
    error("error on conn open");
    return;
  }
  
  sconn_t *conn = marten_conn_init(server->pool);
  
  if (conn == NULL) {
    error("too many connections");
    return;
  }

  info("conn %p: open", conn);
  
  conn->server = server;
  conn->response.write = conn_send;
  
  conn_reset(conn);
  
  uv_tcp_init(uv_default_loop(), &conn->handle);
  
  if (uv_accept((uv_stream_t*)&server->handle, (uv_stream_t*)&conn->handle) == 0) {
    debug("conn %p: uv_read_start()", conn);
    
    uv_read_start((uv_stream_t*)&conn->handle, alloc_buffer, on_conn_read);
  } else {
    uv_close((uv_handle_t*)&conn->handle, on_conn_close);
  }
}

marten_define_pool_static(example, sconn_t, N_CONNECTIONS);

int
main(void) {
  info("sizeof(sconn_t) == %zu", sizeof(sconn_t));
  info("sizeof(sconn_t) - sizeof(uv_tcp_t) - sizeof(uv_write_t) == %zu",
       sizeof(sconn_t) - sizeof(uv_tcp_t) - sizeof(uv_write_t));
  info("sizeof(sconn_t) - sizeof(uv_tcp_t) - sizeof(uv_write_t) - BUFFER_LENGTH == %zu",
       sizeof(sconn_t) - sizeof(uv_tcp_t) - sizeof(uv_write_t) - BUFFER_LENGTH);
  
  server_t static_server = {
    .pool = marten_provide_pool_static(example),
    .terminal = {
      .ptr = static_server.terminal.buf,
      .len = 0,
    }
  };
  
  server_t *server = &static_server;
  
  terminal_push(&server->terminal, "Hello\n", sizeof("Hello\n") - 1);
  
  uv_tcp_init(uv_default_loop(), &server->handle);
  
  struct sockaddr_in bind_addr;
  uv_ip4_addr("0.0.0.0", 8000, &bind_addr);
  
  uv_tcp_bind(&server->handle, (struct sockaddr*)&bind_addr, 0);
  
  uv_tcp_nodelay(&server->handle, 0);
  
  int r = uv_listen((uv_stream_t*)&server->handle, N_CONNECTIONS, on_conn_open);
  if (r) {
    error("error on listen");
    return 1;
  }
  
  uv_run(uv_default_loop(), UV_RUN_DEFAULT);
  
  return 0;
}
