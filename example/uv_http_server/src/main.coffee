#
# Base controller
#

class Base
  languages:
    en:
      title: "English"
      plural: (n)->
        n |= 0
        # nplurals=2; plural=(n != 1);
        if n isnt 1 then 1 else 0
    ru:
      title: "Русский"
      plural: (n)->
        n |= 0
        # nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
        switch
          when n % 10 is 1 and n % 100 isnt 11 then 0
          when n % 10 >= 2 and n % 10 <= 4 and (n % 100 < 10 or n % 100 >= 20) then 1
          else 2

  $language: (navigator?.userLanguage or navigator?.language or "en").replace /[\-\_].+$/, ""

  constructor: ->
    do @switchLanguage

  switchLanguage: (language)->
    @language = if language? and language of @languages then language else @$language
    @plural = @languages[@language].plural

#
# Modules
#

client_modules = [
  require "./terminal" if "terminal" in CLIENT_MODULES
]

views = for {Ctrl, view} in client_modules
  Base = Ctrl Base
  view

#
# Controller
#

class @Main extends Base
  constructor: ->
    super
    @view = 0

  selectView: (@view)=>
    do @$render

#
# View
#

{body, nav, header, article, div, ul, li, a} = require "./node"

viewStyle =
  opacity: 0
  "margin-top": "-100px"
  transition: "1s"
  delayed:
    opacity: 1
    "margin-top": "0px"
  remove:
    opacity: 0
    "margin-top": "100px"

@Root = (opts)->
  body null,
    header
      class:
        header: on
      attrs:
        id: "menu"
      nav
        class:
          "pure-menu": on
          "pure-menu-horizontal": on
        a
          class:
            "pure-menu-heading": on
          attrs:
            href: "#"
          opts.name
        ul
          class:
            "pure-menu-list": on
          for view, id in views
            li
              key: id
              class:
                "pure-menu-item": on
                "pure-menu-selected": opts.view is id
              a
                key: id
                class:
                  "pure-menu-link": on
                attrs:
                  href: "#"
                on:
                  click: do (id)-> -> opts.selectView id
                #"ev-click": showView
                view.title[opts.language]
    
    article
      class:
        "pure-g": on
      div
        class:
          "pure-u-1-24": on
          "pure-u-sm-1-12": on
          "pure-u-md-1-8": on
          "pure-u-lg-1-6": on
          "pure-u-xl-1-5": on
      div
        class:
          "content": on
          "pure-u-11-12": on
          "pure-u-sm-5-6": on
          "pure-u-md-3-4": on
          "pure-u-lg-2-3": on
          "pure-u-xl-3-5": on
        attrs:
          id: "content"
        div
          key: opts.view
          style:
            overflow: "hidden"
          div
            style: viewStyle
            views[opts.view].view opts
