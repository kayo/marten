#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_ERROR
#endif

#define LOG_OFF 0
#define LOG_ERROR 1
#define LOG_INFO 2
#define LOG_DEBUG 3

#define COLOR_RESET "\e[0m"
#define COLOR_RED "\e[0;31m"
#define COLOR_GREEN "\e[0;32m"
#define COLOR_YELLOW "\e[0;33m"
#define COLOR_CYAN "\e[0;36m"

#define log(clr, fmt, ...) fprintf(stderr, clr fmt COLOR_RESET "\n", ##__VA_ARGS__)

#if LOG_LEVEL < LOG_ERROR
#define error(...)
#else
#define error(...) log(COLOR_RED, __VA_ARGS__)
#endif

#if LOG_LEVEL < LOG_INFO
#define info(...)
#else
#define info(...) log(COLOR_GREEN, __VA_ARGS__)
#endif

#if LOG_LEVEL < LOG_DEBUG
#define debug(...)
#else
#define debug(...) log(COLOR_YELLOW, __VA_ARGS__)
#endif

#if LOG_LEVEL < LOG_DEBUG
#define rawlog(...)
#else
#define rawlog(fmt, ptr, len, ...) { \
    char str[len + 1];               \
    memcpy(str, ptr, len);           \
    str[len] = '\0';                 \
    debug(fmt " [["                  \
          COLOR_CYAN "%s"            \
          COLOR_YELLOW "]]",         \
          ##__VA_ARGS__, str);       \
  }
#endif
