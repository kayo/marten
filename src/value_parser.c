#include "common.h"
#include "parser.h"
#include "types.h"

#include <string.h>

int _marten_str_parse(char *data, size_t size,
                      marten_flags_t flags,
                      const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    /* initialize string */
    data[0] = '\0';
  } else {
    size_t offset = strlen(data);
    data += offset;
    size -= offset;
  }

  size_t max = len;
  if (max > size - 1) {
    max = size - 1;
  }
  
  memcpy(data, ptr, max);
  data[max] = '\0';
  
  return len - max;
}

int _marten_uint_parse(void *data, uint8_t size,
                       marten_flags_t flags,
                       const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    switch (size) {
#define T(type) case sizeof(type): *(type*)data = 0; break;
      MARTEN_UINT_TYPES(T);
#undef T
    }
  }
  
  if (ptr == NULL) {
    return 0;
  }
  
  const char *end = ptr + len;
  
  for (; ptr < end; ptr++) {
    if (!isdec(*ptr)) {
      return 1;
    }
    
    switch (size) {
#define T(type) case sizeof(type): {                               \
                  type tmp = *(type*)data;                         \
                  *(type*)data = *(type*)data * 10 + dec2i(*ptr);  \
                  if (*(type*)data < tmp) return 1; /* overflow */ \
                } break;
      MARTEN_UINT_TYPES(T);
#undef T
    }
  }
  
  return 0;
}

int _marten_uint_hex_parse(void *data, uint8_t size,
                           marten_flags_t flags,
                           const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    switch (size) {
#define T(type) case sizeof(type): *(type*)data = 0; break;
      MARTEN_UINT_TYPES(T);
#undef T
    }
  }
  
  if (ptr == NULL) {
    return 0;
  }
  
  const char *end = ptr + len;
  
  for (; ptr < end; ptr++) {
    if (!ishex(*ptr)) {
      return 1;
    }
    
    switch (size) {
#define T(type) case sizeof(type): {                               \
                  type tmp = *(type*)data;                         \
                  *(type*)data = *(type*)data * 10 + hex2i(*ptr);  \
                  if (*(type*)data < tmp) return 1; /* overflow */ \
                } break;
      MARTEN_UINT_TYPES(T);
#undef T
    }
  }
  
  return 0;
}

enum {
  _marten_sint_init = 0,
  _marten_sint_dig = 1 << 0,
  _marten_sint_neg = 1 << 1,
};

int _marten_sint_parse(marten_sint_parser_t *state,
                       void *data, uint8_t size,
                       marten_flags_t flags,
                       const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    *state = _marten_sint_init;
    switch (size) {
#define _(type) case sizeof(type): *(type*)data = 0; break
      _(int8_t);
      _(int16_t);
      _(int32_t);
      _(int64_t);
#undef _
    }
  }
  
  if (ptr == NULL) {
    return 0;
  }
  
  const char *end = ptr + len;
  
  if (!(*state & _marten_sint_dig) && ptr < end) {
    if (*ptr == '-') {
      *state |= _marten_sint_neg;
      ptr++;
    }
    *state |= _marten_sint_dig;
  }
  
  for (; ptr < end; ptr++) {
    if (!isdec(*ptr)) {
      return 1;
    }
    
    switch (size) {
#define _(type) case sizeof(type): {                               \
                  type tmp = *(type*)data;                         \
                  *(type*)data = *(type*)data * 10 + dec2i(*ptr);  \
                  if (*(type*)data < tmp) return 1; /* overflow */ \
                } break
      _(int8_t);
      _(int16_t);
      _(int32_t);
      _(int64_t);
#undef _
    }
  }

  if (marten_has(flags, marten_done) && (*state & _marten_sint_neg)) {
    switch (size) {
#define _(type) case sizeof(type): *(type*)data = -*(type*)data; break
      _(int8_t);
      _(int16_t);
      _(int32_t);
      _(int64_t);
#undef _
    }
  }
  
  return 0;
}
