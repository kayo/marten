#ifndef MARTEN_H
#define MARTEN_H "marten.h"

/**
 * @defgroup top Marten HTTP Construction Kit
 * @brief This library implements basic HTTP processing components.
 *
 * This components can be used for developing effective and compact
 * client and server applications.
 *
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifndef MARTEN_MAX_HEADER_LEN
  /**
   * @brief The maximum request/response header length in bytes
   *
   * This limit is used to prevent header-flood attacks.
   */
#define MARTEN_MAX_HEADER_LEN (80<<10) /* 80K */
#endif

#ifndef MARTEN_MAX_MESSAGE_LEN
  /**
   * @brief The maximum message length in bytes
   *
   * This limit is used to prevent flood attacks.
   */
#define MARTEN_MAX_MESSAGE_LEN (8<<20) /* 8M */
#endif

#ifndef MARTEN_HAS_HW_MUL
  /**
   * @brief The hardware has the fast multiplication instruction
   *
   * This option helps optimize hash calculation for low-end hardware.
   */
#define MARTEN_HAS_HW_MUL 1
#endif

#ifndef MARTEN_HASH_TYPE
  /**
   * @brief The hash function
   */
#define MARTEN_HASH_TYPE djb2
#endif

#ifndef MARTEN_USE_PP
  /**
   * @brief Use marten preprocessor to process sources
   *
   */
#define MARTEN_USE_PP 0
#endif

#define MARTEN_EVAL0(...) __VA_ARGS__
#define MARTEN_EVAL1(...) MARTEN_EVAL0(MARTEN_EVAL0(MARTEN_EVAL0(__VA_ARGS__)))
#define MARTEN_EVAL2(...) MARTEN_EVAL1(MARTEN_EVAL1(MARTEN_EVAL1(__VA_ARGS__)))
#define MARTEN_EVAL3(...) MARTEN_EVAL2(MARTEN_EVAL2(MARTEN_EVAL2(__VA_ARGS__)))
#define MARTEN_EVAL4(...) MARTEN_EVAL3(MARTEN_EVAL3(MARTEN_EVAL3(__VA_ARGS__)))
#define MARTEN_EVAL(...)  MARTEN_EVAL4(MARTEN_EVAL4(MARTEN_EVAL4(__VA_ARGS__)))

#define MARTEN_MAP_END(...)
#define MARTEN_MAP_OUT
#define MARTEN_MAP_COMMA ,

#define MARTEN_MAP_GET_END2() 0, MARTEN_MAP_END
#define MARTEN_MAP_GET_END1(...) MARTEN_MAP_GET_END2
#define MARTEN_MAP_GET_END(...) MARTEN_MAP_GET_END1
#define MARTEN_MAP_NEXT0(test, next, ...) next MARTEN_MAP_OUT
#define MARTEN_MAP_NEXT1(test, next) MARTEN_MAP_NEXT0(test, next, 0)
#define MARTEN_MAP_NEXT(test, next)  MARTEN_MAP_NEXT1(MARTEN_MAP_GET_END test, next)

#define MARTEN_MAP0(f, x, peek, ...) f(x) MARTEN_MAP_NEXT(peek, MARTEN_MAP1)(f, peek, __VA_ARGS__)
#define MARTEN_MAP1(f, x, peek, ...) f(x) MARTEN_MAP_NEXT(peek, MARTEN_MAP0)(f, peek, __VA_ARGS__)

/**
 * Applies the function macro `f` to each of the remaining parameters.
 */
#define MARTEN_MAP(f, ...) MARTEN_EVAL(MARTEN_MAP1(f, __VA_ARGS__, ()()(), ()()(), ()()(), 0))


  /**
   * @brief Get container using pointer to field
   */
#define marten_containerof(ptr, type, member) ((type*)((char*)(ptr) - offsetof(type, member)))

  /**
   * @brief The common 32-bit hash type
   */
  typedef uint32_t marten_hash_t;
  
#define MARTEN_HASH_CALL(a) _MARTEN_HASH_CALL(MARTEN_HASH_TYPE_##a##_, MARTEN_HASH_TYPE)
#define _MARTEN_HASH_CALL(p, t) _MARTEN_HASH_CALL_(p, t)
#define _MARTEN_HASH_CALL_(p, t) p##t

#define MARTEN_HASH_TYPE_init_djb2 5381
#if MARTEN_HAS_HW_MUL
#define MARTEN_HASH_TYPE_hash_djb2(h, c) h * 33 + c
#else
#define MARTEN_HASH_TYPE_hash_djb2(h, c) (h << 5) + h + c
#endif

#define MARTEN_HASH_TYPE_init_sdbm 0
#if MARTEN_HAS_HW_MUL
#define MARTEN_HASH_TYPE_hash_sdbm(h, c) h * 65599 + c
#else
#define MARTEN_HASH_TYPE_hash_sdbm(h, c) (h << 6) + (h << 16) - h + c
#endif

  /**
   * @brief Calculate hash of constant string in compile time
   *
   * @param s Source string
   * @return Result hash
   *
   * Because in common case we cannot calculate constant hashes at compile time, we need to use martenpp preprocessor.
   * This macro needed for tools like *flymake-mode*.
   */
  static inline marten_hash_t marten_hash_cstr(const char *s) {
    marten_hash_t h;
    h = MARTEN_HASH_CALL(init);
    for (; *s != '\0'; )
      h = MARTEN_HASH_CALL(hash)(h, *s++);
    return h;
  }
  
  /**
   * @brief Calculate hash of non-constant string in run time
   *
   * @param s Source string
   * @return Result hash
   */
  marten_hash_t marten_hash_str(const char *s);

  /**
   * @brief Calculate hash of generic string with compile-time optimization
   *
   * @param s Source string
   * @return Result hash
   */
#define marten_hash(s)       \
  (__builtin_constant_p(s) ? \
   marten_hash_cstr(s) :     \
   marten_hash_str(s))
  
  /**
   * @brief The handler flags
   */
  typedef enum {
    /**
     * @brief The none flag
     *
     * This flag used by default in some cases
     */
    marten_none = 0,

    /**
     * @brief The entity beginning flag
     *
     * Each entity have beginning and end.
     * This flags helps control current state of entity content.
     */
    marten_init = 1 << 6,
    /**
     * @brief The entity end flag
     *
     * Each entity have beginning and end.
     * This flags helps control current state of entity content.
     */
    marten_done = 1 << 7,

    /**
     * @brief The field flag
     *
     * Some parsers like head parser returns field-value entities.
     * Checking this flag can helps you determine current entity.
     */
    marten_field = 1 << 4,
    /**
     * @brief The value flag
     *
     * Some parsers like head parser returns field-value entities.
     * Checking this flag can helps you determine current entity.
     */
    marten_value = 1 << 5,

    /**
     * @brief The field or value flag
     */
    marten_entry = marten_field | marten_value,

    /**
     * @brief The request method entity flag
     */
    marten_method = 1,
    /**
     * @brief The request path entity flag
     */
    marten_path = 2,
    /**
     * @brief The request query entity flag
     */
    marten_query = 3,
    
    /**
     * @brief The response status entity flag
     */
    marten_status = 1,
    /**
     * @brief The response message entity flag
     */
    marten_message = 2,
    
    /**
     * @brief The request/response protocol entity flag
     */
    marten_proto = 4,
    /**
     * @brief The request/response header entity flag
     */
    marten_head = 5,
    /**
     * @brief The request/response body entity flag
     */
    marten_body = 6,
    /**
     * @brief The request/response body content entity flag
     */
    marten_content = 7,
    
    marten_entity = marten_method | marten_path | marten_query | marten_proto | marten_head | marten_body | marten_content,
  } marten_flags_t;
  
  /**
   * @brief Check the state of required flags
   *
   * @param actual_flags Type marten_flags_t The actual flags
   * @param required_flags Type marten_flags_t The required flags
   * @return Type bool The state of required flags
   */
#define marten_has(actual_flags, required_flags) ((actual_flags) & (required_flags))

  /**
   * @brief Check the state of required entity
   *
   * @param actual_flags Type marten_flags_t The actual flags
   * @param required_entity Type marten_flags_t The required entity
   * @return Type bool The state of required entity
   */
#define marten_is(actual_flags, required_entity) (((actual_flags) & marten_entity) == (required_entity))
  
  /**
   * @brief The common state
   */
  typedef void marten_state_t;
  
  /**
   * @brief The common handler callback
   *
   * @param state Current parser state
   * @param flags Current parsing flags
   * @param pre Data chunk pointer
   * @param len Data chunk length
   * @return Parsing status
   */
  typedef int
  marten_handler_t(marten_state_t *state,
                   marten_flags_t flags,
                   const char *ptr,
                   size_t len);
  
  /**
   * @brief Define the handler callback
   *
   * @param name The name of handler
   * @return Handler function definition
   */
#define marten_define_handler(name) \
  int name(marten_state_t *state,   \
           marten_flags_t flags,    \
           const char *ptr,         \
           size_t len)
  
  /**
   * @brief The http request limiter state
   */
  typedef uint32_t marten_limiter_t;

  /**
   * @brief The http request parser state
   */
  typedef uint8_t marten_request_parser_t;

  /**
   * @brief The http request limiter
   *
   * Use this pseudo-parser to prevent flood attacks.
   */
  int marten_request_limit(marten_limiter_t *offset,
                           int32_t header_limit,
                           int32_t message_limit,
                           marten_request_parser_t *state,
                           marten_handler_t *handler,
                           marten_flags_t flags,
                           const char *ptr, size_t len);

  /**
   * @brief The http request parser
   *
   * Use this parser to parse HTTP request data.
   *
   * @param state The pointer to request parser state
   * @param handler The pointer to handler function
   * @param flags The data chunk control flags
   * @param ptr The pointer to data chunk
   * @param len The length of data chunk in bytes
   * @return Parsing status
   */
  int marten_request_parse(marten_request_parser_t *state,
                           marten_handler_t *handler,
                           marten_flags_t flags,
                           const char *ptr, size_t len);

  /**
   * @brief The http response parser state
   */
  typedef uint8_t marten_response_parser_t;

  /**
   * @brief The http response limiter
   *
   * Use this pseudo-parser to prevent flood attacks.
   */
  int marten_response_limit(marten_limiter_t *offset,
                            int32_t header_limit,
                            int32_t message_limit,
                            marten_response_parser_t *state,
                            marten_handler_t *handler,
                            marten_flags_t flags,
                            const char *ptr, size_t len);

  /**
   * @brief The http response parser
   *
   * Use this parser to parse HTTP response data.
   *
   * @param state The pointer to response parser state
   * @param handler The pointer to handler function
   * @param flags The data chunk control flags
   * @param ptr The pointer to data chunk
   * @param len The length of data chunk in bytes
   * @return Parsing status
   */
  int marten_response_parse(marten_response_parser_t *state,
                            marten_handler_t *handler,
                            marten_flags_t flags,
                            const char *ptr, size_t len);

  /**
   * @brief The header parser state
   */
  typedef uint8_t marten_head_parser_t;

  /**
   * @brief The http header parser
   *
   * Use this parser to parse HTTP request/response header.
   *
   * @param state The pointer to header parser state
   * @param handler The pointer to handler function
   * @param flags The data chunk control flags
   * @param ptr The pointer to data chunk
   * @param len The length of data chunk in bytes
   * @return Parsing status
   */
  int marten_head_parse(marten_head_parser_t *state,
                        marten_handler_t *handler,
                        marten_flags_t flags,
                        const char *ptr, size_t len);
  
  /**
   * @brief The query string parser state
   */
  typedef uint8_t marten_query_parser_t;

  /**
   * @brief The http query string parser
   *
   * Use this parser to parse HTTP request query string.
   *
   * @param state The pointer to query string parser state
   * @param handler The pointer to handler function
   * @param flags The data chunk control flags
   * @param ptr The pointer to data chunk
   * @param len The length of data chunk in bytes
   * @return Parsing status
   */
  int marten_query_parse(marten_query_parser_t *state,
                         marten_handler_t *handler,
                         marten_flags_t flags,
                         const char *ptr, size_t len);

#define marten_is_raw(flags, entity) \
  (marten_is(flags, entity) &&       \
   !marten_has(flags, marten_field | \
               marten_value))

#define marten_is_ent(flags, entity) \
  (marten_is(flags, entity) &&       \
   marten_has(flags, marten_field |  \
              marten_value))
  
  /**
   * @brief The query string decoder state
   */
  typedef uint8_t marten_query_decoder_t;

  /**
   * @brief The http query string decoder
   *
   * Use this parser to decode HTTP request query string.
   *
   * @param state The pointer to query string decoder state
   * @param handler The pointer to handler function
   * @param flags The data chunk control flags
   * @param ptr The pointer to data chunk
   * @param len The length of data chunk in bytes
   * @return Parsing status
   */
  int marten_query_decode(marten_query_decoder_t *state,
                          marten_handler_t *handler,
                          marten_flags_t flags,
                          const char *ptr, size_t len);

  /**
   * @brief Update the hash of source data
   *
   * @param hash Pointer to target hash
   * @param flags The flags
   * @param ptr The pointer to source data
   * @param len The length of source data
   */
  void marten_hash_update(marten_hash_t *hash,
                          marten_flags_t flags,
                          const char *ptr, size_t len);

  /**
   * @brief The field picker helper
   *
   * Use this pseudo-parser to calculate hash of entity field.
   *
   * @param hash Pointer to target hash
   * @param flags The flags
   * @param ptr The pointer to source data
   * @param len The length of source data
   */
  static inline void marten_field_pick(marten_hash_t *hash,
                                       marten_flags_t flags,
                                       const char *ptr, size_t len) {
    if (marten_has(flags, marten_field)) {
      marten_hash_update(hash, flags, ptr, len);
    }
  }

  /**
   * @brief The value picker helper
   *
   * Use this pseudo-parser to calculate hash of entity value.
   *
   * @param hash Pointer to target hash
   * @param flags The flags
   * @param ptr The pointer to source data
   * @param len The length of source data
   */
  static inline void marten_value_pick(marten_hash_t *hash,
                                       marten_flags_t flags,
                                       const char *ptr, size_t len) {
    if (marten_has(flags, marten_value)) {
      marten_hash_update(hash, flags, ptr, len);
    }
  }

  /**
   * @brief The state flags of connection
   */
  enum {
    /**
     * @brief Connection in use
     */
    marten_conn_used = 1 << 0,
    /**
     * @brief Connection keep alive
     */
    marten_conn_keep = 1 << 1,
    /**
     * @brief Connection receives data
     */
    marten_conn_recv = 1 << 2,
    /**
     * @brief Connection sends data
     */
    marten_conn_send = 1 << 3,
    /**
     * @brief Connection has partial content (chunked transfer)
     */
    marten_conn_part = 1 << 4,
    /**
     * @brief Connection is used for data transfer (WebSocket or SSE)
     */
    marten_conn_data = 1 << 5,
  };

#define marten_set(state, flags) ((state) |= (flags))
#define marten_reset(state, flags) ((state) &= ~(flags))
  
  /**
   * @brief The state of connection
   */
  typedef uint8_t marten_conn_state_t;
  
  /**
   * @brief The connection
   */
  typedef void marten_conn_t;
  
  /**
   * @brief The connection pool
   */
  typedef struct marten_pool marten_pool_t;
  
  /**
   * @brief The connection initializer function
   *
   * @param pool The target connection pool
   * @return The new connection or `NULL`
   *
   * The `NULL` can be returned to indicate exceeding the limit of simultaneous connection.
   */
  typedef marten_conn_t *marten_conn_init_t(const marten_pool_t *pool);
  
  /**
   * @brief The connection finalizer function
   *
   * @param pool The target connection pool
   * @param conn The connection to finalize
   */
  typedef void marten_conn_done_t(const marten_pool_t *pool,
                                  marten_conn_t *conn);

  /**
   * @brief The pool iterator
   *
   * @param pool The target connection pool
   * @param conn The current connection
   * @return The next connection
   *
   * To get the first connection set @p conn to `NULL`.
   */
  typedef marten_conn_t *marten_conn_next_t(const marten_pool_t *pool,
                                            const marten_conn_t *conn);
  
  /**
   * @brief The basic connection pool
   */
  struct marten_pool {
    /**
     * @brief The connection initializer
     */
    marten_conn_init_t *conn_init;
    /**
     * @brief The connection finalizer
     */
    marten_conn_done_t *conn_done;
    /**
     * @brief The connection iterator
     */
    marten_conn_next_t *conn_next;
  };

  /**
   * @brief The connection initializer
   *
   * @param pool The target connection pool
   * @return The new connection
   *
   * The `NULL` value returned means what limit of simultaneous connetions are exceeded.
   */
  static inline marten_conn_t *
  marten_conn_init(const marten_pool_t *pool) {
    return pool->conn_init(pool);
  }
  
  /**
   * @brief The connection finalizer
   *
   * @param pool The target connection pool
   * @param conn The connection to finalize
   */
  static inline void
  marten_conn_done(const marten_pool_t *pool,
                   marten_conn_t *conn) {
    pool->conn_done(pool, conn);
  }

  /**
   * @brief The connection iterator
   *
   * @param pool The target connection pool
   * @param conn The current connection
   * @return The next connection
   *
   * Use this function to iterate over all active connections in pool.
   * To get the first connection set @p conn to `NULL`.
   */
  static inline marten_conn_t *
  marten_conn_next(const marten_pool_t *pool,
                   const marten_conn_t *conn) {
    return pool->conn_next(pool, conn);
  }
  
  /**
   * @brief The static connection pool
   */
  typedef struct {
    marten_pool_t conn_pool;
    marten_conn_t *pool_base;
    size_t conn_size;
    size_t pool_size;
  } marten_pool_static_t;

  marten_conn_t *marten_conn_init_static(const marten_pool_t *pool);
  void marten_conn_done_static(const marten_pool_t *pool,
                               marten_conn_t *conn);
  marten_conn_t *marten_conn_next_static(const marten_pool_t *pool,
                                         const marten_conn_t *conn);
  
#define marten_define_pool_static(pool_name, conn_type, max_conns) \
  static conn_type pool_name##_pool[max_conns];                  \
  const marten_pool_static_t pool_name##_inst = {                \
    { marten_conn_init_static,                                   \
      marten_conn_done_static,                                   \
      marten_conn_next_static },                                 \
    (marten_conn_t*)pool_name##_pool,                            \
    sizeof(conn_type),                                           \
    max_conns,                                                   \
  }

#define marten_provide_pool_static(pool_name) (&pool_name##_inst.conn_pool)
  
  /**
   * @code
   *   marten_define_sconn_pool(app, sizeof(union_sconn_t), 64)
   * @endcode
   */

  enum {
    marten_route_none = (1 << 8) - 1
  };
  
  typedef uint8_t marten_route_t;

  typedef marten_route_t marten_route_match_t(marten_hash_t hash);
  
  typedef int marten_route_apply_t(marten_route_t *route,
                                   marten_flags_t flags,
                                   const char *ptr, size_t len);

  typedef struct {
    marten_route_match_t *match;
    marten_route_apply_t *apply;
  } marten_router_t;

#define _marten_define_route_(method, path, handler) \
  marten_route_##handler,
#define _marten_define_route(route) \
  _marten_define_route_ route

#define _marten_route_hash(method, path)        \
  marten_hash_cstr(method path)
  
#define _marten_match_route_(method, path, handler) \
  _marten_route_hash(method, path) == hash ?        \
    marten_route_##handler :
#define _marten_match_route(route) \
  _marten_match_route_ route

#define _marten_apply_route_(method, path, handler)  \
  *route == marten_route_##handler ?                 \
    handler(route, flags, ptr, len) :
#define _marten_apply_route(route) \
  _marten_apply_route_ route
  
#define _marten_define_router_(name, ...)            \
  enum {                                             \
    MARTEN_MAP(_marten_define_route, ##__VA_ARGS__)  \
  };                                                 \
  static marten_route_t                              \
  name##_match(marten_hash_t hash) {                 \
    return                                           \
      MARTEN_MAP(_marten_match_route, ##__VA_ARGS__) \
      marten_route_none;                             \
  }                                                  \
  static int                                         \
  name##_apply(marten_route_t *route,                \
               marten_flags_t flags,                 \
               const char *ptr,                      \
               size_t len) {                         \
    return                                           \
      MARTEN_MAP(_marten_apply_route, ##__VA_ARGS__) \
      0;                                             \
  }                                                  \
  const marten_router_t name = {                     \
    name##_match,                                    \
    name##_apply,                                    \
  }

  /**
   * @brief Define simple static path-based router
   *
   * @param name The name of router
   * @param ... The routes
   * @return The router definition
   * 
   * Each route must have method, path and handler.
   * See short example below:
   *
   * @code{.c}
   *   marten_define_router(example,
   *                        ("GET", "/", root_get_handler)
   *                        ("PUT", "/example", exaple_put_handler));
   * @endcode
   *
   * Also see @p uv_http_server.c example.
   */
#define marten_define_router(name, ...)         \
  _marten_define_router_(name, ##__VA_ARGS__)
  
  int marten_router_handle(const marten_router_t *router,
                           marten_route_t *route,
                           marten_hash_t *hash,
                           marten_flags_t flags,
                           const char *ptr, size_t len);

  static inline marten_route_t
  _marten_router_route(const marten_router_t *router,
                       marten_hash_t hash) {
    return router->match(hash);
  }

#define marten_router_route(router, method, path)                 \
  _marten_router_route(router, _marten_route_hash(method, path))

  /**
   * The value's parsers
   */
  
  int _marten_str_parse(char *data, size_t size,
                        marten_flags_t flags,
                        const char *ptr, size_t len);

#define marten_str_parse(data, flags, ptr, len) \
  _marten_str_parse(data, sizeof(data), flags, ptr, len)
  
  int _marten_uint_parse(void *data, uint8_t size,
                         marten_flags_t flags,
                         const char *ptr, size_t len);

#define marten_uint_parse(data, flags, ptr, len) \
  _marten_uint_parse(&(data), sizeof(data), flags, ptr, len)

  int _marten_uint_hex_parse(void *data, uint8_t size,
                             marten_flags_t flags,
                             const char *ptr, size_t len);

#define marten_uint_hex_parse(data, flags, ptr, len)              \
  _marten_uint_hex_parse(&(data), sizeof(data), flags, ptr, len)
  
  typedef uint8_t marten_sint_parser_t;
  
  int _marten_sint_parse(marten_sint_parser_t *state,
                         void *data, uint8_t size,
                         marten_flags_t flags,
                         const char *ptr, size_t len);

#define marten_sint_parse(state, data, flags, ptr, len)       \
  _marten_sint_parse(state, &(data), sizeof(data), flags, ptr, len)

  /**
   * Response
   */
  typedef uint32_t marten_stage_t;

  typedef struct marten_response_writer marten_response_writer_t;
  
  typedef void
  marten_writer_t(marten_response_writer_t *writer);
  
  struct marten_response_writer {
    marten_handler_t *write;
    marten_writer_t *writer;
    marten_stage_t stage;
  };

#define marten_define_writer(name)              \
  void name(marten_response_writer_t *writer)

#define marten_begin(writer)                    \
  switch(writer->stage) { case 1:

#define marten_yield(writer, ...)                   \
  do { writer->stage = (__LINE__ << 4) __VA_ARGS__; \
    return; case (__LINE__ << 4) __VA_ARGS__:;      \
  } while (0)

#define marten_end(writer) } writer->stage = 0

  static inline int marten_resume_write(marten_response_writer_t *writer) {
    if (writer->stage != 0) {
      writer->writer(writer);
      return writer->stage == 0 ? 1 : 0;
    }
    return 1;
  }
  
  static inline void marten_respond(marten_response_writer_t *writer,
                                    marten_writer_t *response) {
    writer->writer = response;
    writer->stage = 1;
    writer->writer(writer);
  }

#define marten_feed "\r\n"

#define marten_raw_write(writer, data, size, ...) \
  writer->write(writer, marten_none, data, size); \
  marten_yield(writer, __VA_ARGS__)

#define marten_status_cstr_write(writer, proto, status, message, ...) \
  marten_cstr_write(writer, proto " " #status " " message marten_feed, ##__VA_ARGS__)

#define marten_header_cstr_write(writer, field, value, ...)             \
  marten_cstr_write(writer, field ": " value marten_feed, ##__VA_ARGS__)

#define marten_feed_write(writer, ...)                  \
  marten_cstr_write(writer, marten_feed, ##__VA_ARGS__)

#define marten_conn_header_write(writer, state)                   \
  if (marten_has(state, marten_conn_keep)) {                      \
    marten_header_cstr_write(writer, "Connection", "keep-alive"); \
  } else {                                                        \
    marten_header_cstr_write(writer, "Connection", "close", +1);  \
  }

#ifdef MARTEN_CSTR_ATTR
#define marten_cstr_write(writer, str, ...) do {                    \
    static const char cstr[] MARTEN_CSTR_ATTR = str;                \
    marten_raw_write(writer, cstr, sizeof(str) - 1, ##__VA_ARGS__); \
  } while(0)
#else
#define marten_cstr_write(writer, str, ...)                     \
  marten_raw_write(writer, str, sizeof(str) - 1, ##__VA_ARGS__)
#endif

#define marten_str_write(writer, str, ...)        \
  marten_raw_write(writer, str, strlen(str), ##__VA_ARGS__)

  static inline char *marten_raw_tostr(char *buf, const void *ptr, size_t len) {
    memcpy(buf, ptr, len);
    return buf + len;
  }

#define marten_cstr_tostr(buf, str)             \
  (marten_raw_tostr(buf, str, sizeof(str)) - 1)
  
  static inline char *marten_str_tostr(char *buf, const char *str) {
    return marten_raw_tostr(buf, str, strlen(str) + 1) - 1;
  }
  
  typedef uint32_t marten_uint_t;
  typedef int32_t marten_sint_t;
  
  char *marten_uint_tostr(char *buf, marten_uint_t val);
  char *marten_uint_hex_tostr(char *buf, marten_uint_t val);
  char *marten_sint_tostr(char *buf, marten_sint_t);
  
#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* MARTEN_H */
