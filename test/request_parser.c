#include <marten.h>

#include <stdio.h>
#include <string.h>

typedef struct {
  marten_flags_t flags;
  const char *data;
} test_token_t;

#define token(data, flags) { flags, data },

#define method(name, ...) token(name, marten_method __VA_ARGS__)
#define path(name, ...) token(name, marten_path __VA_ARGS__)
#define query(name, ...) token(name, marten_query __VA_ARGS__)
#define proto(name, ...) token(name, marten_proto __VA_ARGS__)
#define head(name, ...) token(name, marten_head __VA_ARGS__)
#define body(name, ...) token(name, marten_body __VA_ARGS__)

#define chunk(...) __VA_ARGS__,

#define field | marten_field
#define value | marten_value

#define init | marten_init
#define done | marten_done

typedef struct {
  marten_request_parser_t request_parser;
  union {
    marten_query_parser_t query_parser;
    marten_head_parser_t head_parser;
  };
  const test_token_t *ini_token;
  const test_token_t *token;
  const test_token_t *end_token;
  int error;
} test_request_parser_t;

static const char *ent_name(marten_flags_t flags) {
  switch (flags & marten_entity) {
  case marten_method: return "method";
  case marten_path: return "path";
  case marten_query: return "query";
  case marten_proto: return "proto";
  case marten_head: return "head";
  case marten_body: return "body";
  case marten_content: return "content";
  }
  return "unknown";
}

static const char *ent_part(marten_flags_t flags) {
  switch (flags & marten_entry) {
  case marten_field: return " field";
  case marten_value: return " value";
  }
  return "";
}

#define flags_fmt "%s%s%s%s"

#define show_flags(flags)                        \
  (flags & marten_init ? "init " : ""),          \
    (flags & marten_done ? "done " : ""),        \
    ent_name(flags), ent_part(flags)

#define fail(f, ...) fprintf(stderr, "\r\e[1;31mFAIL ):\e[0m\n" f "\n", ##__VA_ARGS__)

static marten_define_handler(test_handler) {
  int is_query = marten_is(flags, marten_query);
  int is_header = marten_is(flags, marten_head);
  int is_param = marten_has(flags, marten_field | marten_value);
  
  test_request_parser_t *test_data =
    (is_query && is_param) ? marten_containerof(state, test_request_parser_t, query_parser) :
    (is_header && is_param) ? marten_containerof(state, test_request_parser_t, head_parser) :
    marten_containerof(state, test_request_parser_t, request_parser);
  
  if (test_data->token < test_data->end_token) {
    char data[len + 1];
    memcpy(data, ptr, len);
    data[len] = '\0';
    
    if (0 != strcmp(data, test_data->token->data)) {
      fail("Invalid token #%lu data! Expected [%s] but actual is [%s]",
           test_data->token - test_data->ini_token,
           test_data->token->data, data);
      goto failed;
    }
    
    if (flags != test_data->token->flags) {
      fail("Invalid token #%lu flags! Expected [" flags_fmt "] but actual is [" flags_fmt "]",
           test_data->token - test_data->ini_token,
           show_flags(test_data->token->flags),
           show_flags(flags));
      goto failed;
    }
  }
  
  test_data->token++;

  int res = 0;

  if (!is_param) {
    if (is_query) {
      res = marten_query_parse(&test_data->query_parser, test_handler, flags, ptr, len);
    } else if (is_header) {
      res = marten_head_parse(&test_data->head_parser, test_handler, flags, ptr, len);
    }
  }
  
  return res;
  
 failed:
  test_data->error = 1;
  return 1;
}

static void
run_test (const char *title, int result,
          size_t chunks, const char **chunk,
          size_t tokens, const test_token_t *token) {
  fprintf(stderr, "\e[1;37m.... ??\e[0m \e[1;34m%s\e[0m ", title);
  
  test_request_parser_t test_parser;
  test_request_parser_t *test_data = &test_parser;
  
  test_data->error = 0;
  test_data->ini_token = test_data->token = token;
  test_data->end_token = token + tokens;
  
  int res = 0;
  const char **ini_chunk = chunk, **end_chunk = chunk + chunks;
  for (; chunk < end_chunk && res == 0; chunk++) {
    res = marten_request_parse(&test_data->request_parser, test_handler,
                               (chunk == ini_chunk ? marten_init : 0) |
                               (chunk + 1 == end_chunk ? marten_done : 0),
                               *chunk, strlen(*chunk));
  }
  
  if (test_data->error == 1) {
    return;
  }
  
  if (res != result) {
    fail("Invalid Result! Expected is %d but actual result is %d", result, res);
    return;
  }
  
  if (test_data->token < test_data->end_token) {
    fail("Underrun detected! Expected %lu tokens but actually produced %lu tokens", tokens, test_data->token - token);
    return;
  }
  
  if (test_data->token > test_data->end_token) {
    fail("Overrun detected! Expected %lu tokens but actually produced %lu tokens", tokens, test_data->token - token);
    return;
  }
  
  fprintf(stderr, "\r\e[1;32mPASS (:\e[0m\n");
}

#define test(title, result, chunks, ...) {                \
    const char *test_chunks[] = { chunks };               \
    const test_token_t test_tokens[] = { __VA_ARGS__ };   \
    run_test(title, result,                               \
             sizeof(test_chunks)/sizeof(test_chunks[0]),  \
             test_chunks,                                 \
             sizeof(test_tokens)/sizeof(test_tokens[0]),  \
             test_tokens);                                \
  }

#define show_type_size(type) printf("sizeof(" #type ") == %lu\n", sizeof(type))

int main(int argc, char *argv[]) {
  (void)argc;
  (void)argv;
  
  show_type_size(marten_request_parser_t);
  show_type_size(marten_query_parser_t);
  show_type_size(marten_head_parser_t);
  show_type_size(test_request_parser_t);
  
  test("Simple GET request", 0,
       chunk("GET / HTTP/1.1\r\n\r\n"),
       method("GET", init done)
       path("/", init done)
       proto("HTTP", init field done)
       proto("1.1", init value done));
  
  test("GET request with path and query-string", 0,
       chunk("GET /path/to/resource?id=12&hash=%10%A4%25%2E%91%F9 HTTP/1.1\r\n")
       chunk("\r\n"),
       method("GET", init done)
       path("/path/to/resource", init done)
       query("id=12&hash=%10%A4%25%2E%91%F9", init done)
       query("id", init field done)
       query("12", init value done)
       query("hash", init field done)
       query("%10%A4%25%2E%91%F9", init value done)
       proto("HTTP", init field done)
       proto("1.1", init value done));
  
  test("GET request with url-encoded path and headers", 0,
       chunk("GET /url%0Enc%0d%ed/p%0Ath HTTP/1.1\r\n")
       chunk("Accept: text/html, application/json\r\n")
       chunk("ETag: a1f28d373e52123d\r\n")
       chunk("Content-Length: 0\r\n")
       chunk("\r\n"),
       method("GET", init done)
       path("/url%0Enc%0d%ed/p%0Ath", init done)
       proto("HTTP", init field done)
       proto("1.1", init value done)
       head("Accept: text/html, application/json\r\n", init)
       head("Accept", init field done)
       head("text/html, application/json", init value done)
       head("ETag: a1f28d373e52123d\r\n")
       head("ETag", init field done)
       head("a1f28d373e52123d", init value done)
       head("Content-Length: 0\r\n")
       head("Content-Length", init field done)
       head("0", init value done)
       head("", done));

  test("Chunked GET request with query-string url-encoded path and headers", 0,
       chunk("GET ")chunk("/url%")chunk("0Enc%0")chunk("d")chunk("%ed/p%0Ath")chunk(" HTTP/")chunk("1.1\r")chunk("\n")
       chunk("Acc")chunk("ept: text/html,")chunk(" application/json")chunk("\r\n")
       chunk("ETag")chunk(": a1f28d373e52123")chunk("d\r\n")
       chunk("Content-Length: ")chunk("900\r")chunk("\n")
       chunk("\r\n"),
       method("GET", init done)
       path("/url%", init)
       path("0Enc%0")
       path("d")
       path("%ed/p%0Ath")
       path("", done)
       proto("HTTP", init field done)
       proto("1.1", init value done)
       head("Acc", init)
       head("Acc", init field)
       head("ept: text/html,")
       head("ept", field done)
       head("text/html,", init value)
       head(" application/json")
       head(" application/json", value)
       head("\r\n")
       head("", value done)
       head("ETag")
       head("ETag", init field)
       head(": a1f28d373e52123")
       head("", field done)
       head("a1f28d373e52123", init value)
       head("d\r\n")
       head("d", value done)
       head("Content-Length: ")
       head("Content-Length", init field done)
       head("900\r")
       head("900", init value done)
       head("\n")
       head("", done));
  
  test("Per-byte-chunked GET request", 0,
       chunk("")chunk("G")chunk("E")chunk("T")chunk(" ")chunk("/")chunk(" ")
       chunk("H")chunk("T")chunk("T")chunk("P")chunk("/")chunk("1")chunk(".")chunk("1")
       chunk("\r")chunk("\n")chunk("\r")chunk("\n"),
       method("G", init)
       method("E")
       method("T")
       method("", done)
       path("/", init)
       path("", done)
       proto("H", init field)
       proto("T", field)
       proto("T", field)
       proto("P", field)
       proto("", field done)
       proto("1", init value)
       proto(".", value)
       proto("1", value)
       proto("", value done));

  test("Real chromium request", 0,
       chunk("GET / HTTP/1.1\r\n"
             "Host: illumium.org\r\n"
             "Connection: keep-alive\r\n"
             "Cache-Control: max-age=0\r\n"
             "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\n"
             "Upgrade-Insecure-Requests: 1\r\n"
             "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36\r\n"
             "Accept-Encoding: gzip, deflate, sdch\r\n"
             "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n"
             "Cookie: sessionid=o12dsfa2ae2rAf4mbo9hfdsg; js=1\r\n"
             "\r\n"),
       method("GET", init done)
       path("/", init done)
       proto("HTTP", init field done)
       proto("1.1", init value done)
       head("Host: illumium.org\r\n"
              "Connection: keep-alive\r\n"
              "Cache-Control: max-age=0\r\n"
              "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\n"
              "Upgrade-Insecure-Requests: 1\r\n"
              "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36\r\n"
              "Accept-Encoding: gzip, deflate, sdch\r\n"
              "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n"
              "Cookie: sessionid=o12dsfa2ae2rAf4mbo9hfdsg; js=1\r\n", init done)
       head("Host", init field done)
       head("illumium.org", init value done)
       head("Connection", init field done)
       head("keep-alive", init value done)
       head("Cache-Control", init field done)
       head("max-age=0", init value done)
       head("Accept", init field done)
       head("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", init value done)
       head("Upgrade-Insecure-Requests", init field done)
       head("1", init value done)
       head("User-Agent", init field done)
       head("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", init value done)
       head("Accept-Encoding", init field done)
       head("gzip, deflate, sdch", init value done)
       head("Accept-Language", init field done)
       head("ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4", init value done)
       head("Cookie", init field done)
       head("sessionid=o12dsfa2ae2rAf4mbo9hfdsg; js=1", init value done));

  test("Simple GET request with body", 0,
       chunk("GET / HTTP/1.1\r\n\r\nBody message"),
       method("GET", init done)
       path("/", init done)
       proto("HTTP", init field done)
       proto("1.1", init value done)
       body("Body message", init done));

  test("Simple GET request with body chunks", 0,
       chunk("GET / HTTP/1.1\r\n")
       chunk("\r\nBody ")
       chunk("message"),
       method("GET", init done)
       path("/", init done)
       proto("HTTP", init field done)
       proto("1.1", init value done)
       body("Body ", init)
       body("message", done));
  
  return 0;
}
