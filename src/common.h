#include <marten.h>

#ifdef __GNUC__
#define force_inline inline __attribute__((always_inline))
#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)
#else
#define force_inline inline
#define likely(x) (x)
#define unlikely(x) (x)
#endif

//#define MARTEN_DEBUG_HTTP

#ifdef MARTEN_DEBUG_HTTP
#include <stdio.h>
#define debug(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#else
#define debug(...)
#endif
