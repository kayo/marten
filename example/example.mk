example.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

example.INHERIT := marten-pp libmarten

example.list ?= bsd_http_server uv_http_server
example.CDIRS := $(example.BASEPATH)
example.ALIGN := 4
example.SIZEB := 4

TARGET.PKGS += libuv
libexample/uv_http_server.INHERIT := libuv
example/uv_http_server.INHERIT := libuv

HTML_DIR := example/uv_http_server
INDEX_HTML_GZ := $(HTML_DIR)/dist/browser/index.html.gz
HTML_DEPS_MK := $(dir $(INDEX_HTML_GZ))depends.mk

libexample/uv_http_server.SRCS += $(INDEX_HTML_GZ)
example/uv_http_server.c: $(INDEX_HTML_H)

$(INDEX_HTML_GZ) $(HTML_DEPS_MK):
	@echo WEBPACK $@
	$(Q)cd $(HTML_DIR) && npm install

-include $(HTML_DEPS_MK)

define MARTEN_EXAMPLE_RULES
TARGET.LIBS += libexample/$(1)
libexample/$(1).INHERIT += example
libexample/$(1).SRCS += $(example.BASEPATH)$(1).c

TARGET.BINS += example/$(1)
example/$(1).INHERIT += example
example/$(1).DEPLIBS* := libmarten libexample/$(1)

example.$(1): run.example/$(1)
endef

$(call ADDRULES,MARTEN_EXAMPLE_RULES:example.list)
