#include <marten.h>

#include <stdio.h>
#include <string.h>

typedef struct {
  marten_flags_t flags;
  const char *data;
} test_token_t;

#define token(data, flags) { flags, data },

#define proto(name, ...) token(name, marten_proto __VA_ARGS__)
#define status(name, ...) token(name, marten_status __VA_ARGS__)
#define message(name, ...) token(name, marten_message __VA_ARGS__)
#define head(name, ...) token(name, marten_head __VA_ARGS__)
#define body(name, ...) token(name, marten_body __VA_ARGS__)

#define chunk(...) __VA_ARGS__,

#define field | marten_field
#define value | marten_value

#define init | marten_init
#define done | marten_done

typedef struct {
  marten_response_parser_t response_parser;
  marten_head_parser_t head_parser;
  const test_token_t *ini_token;
  const test_token_t *token;
  const test_token_t *end_token;
  int error;
} test_response_parser_t;

static const char *ent_name(marten_flags_t flags) {
  switch (flags & marten_entity) {
  case marten_method: return "method";
  case marten_path: return "path";
  case marten_query: return "query";
  case marten_proto: return "proto";
  case marten_head: return "head";
  case marten_body: return "body";
  case marten_content: return "content";
  }
  return "unknown";
}

static const char *ent_part(marten_flags_t flags) {
  switch (flags & marten_entry) {
  case marten_field: return " field";
  case marten_value: return " value";
  }
  return "";
}

#define flags_fmt "%s%s%s%s"

#define show_flags(flags)                        \
  (flags & marten_init ? "init " : ""),          \
    (flags & marten_done ? "done " : ""),        \
    ent_name(flags), ent_part(flags)

#define fail(f, ...) fprintf(stderr, "\r\e[1;31mFAIL ):\e[0m\n" f "\n", ##__VA_ARGS__)

static marten_define_handler(test_handler) {
  int is_header = marten_is(flags, marten_head);
  int is_param = marten_has(flags, marten_field | marten_value);
  
  test_response_parser_t *test_data =
    (is_header && is_param) ? marten_containerof(state, test_response_parser_t, head_parser) :
    marten_containerof(state, test_response_parser_t, response_parser);
  
  if (test_data->token < test_data->end_token) {
    char data[len + 1];
    memcpy(data, ptr, len);
    data[len] = '\0';
    
    if (0 != strcmp(data, test_data->token->data)) {
      fail("Invalid token #%lu data! Expected [%s] but actual is [%s]",
           test_data->token - test_data->ini_token,
           test_data->token->data, data);
      goto failed;
    }
    
    if (flags != test_data->token->flags) {
      fail("Invalid token #%lu flags! Expected [" flags_fmt "] but actual is [" flags_fmt "]",
           test_data->token - test_data->ini_token,
           show_flags(test_data->token->flags),
           show_flags(flags));
      goto failed;
    }
  }
  
  test_data->token++;

  int res = 0;

  if (!is_param) {
    if (is_header) {
      res = marten_head_parse(&test_data->head_parser, test_handler, flags, ptr, len);
    }
  }
  
  return res;
  
 failed:
  test_data->error = 1;
  return 1;
}

static void
run_test (const char *title, int result,
          size_t chunks, const char **chunk,
          size_t tokens, const test_token_t *token) {
  fprintf(stderr, "\e[1;37m.... ??\e[0m \e[1;34m%s\e[0m ", title);
  
  test_response_parser_t test_parser;
  test_response_parser_t *test_data = &test_parser;
  
  test_data->error = 0;
  test_data->ini_token = test_data->token = token;
  test_data->end_token = token + tokens;
  
  int res = 0;
  const char **ini_chunk = chunk, **end_chunk = chunk + chunks;
  for (; chunk < end_chunk && res == 0; chunk++) {
    res = marten_response_parse(&test_data->response_parser, test_handler,
                                (chunk == ini_chunk ? marten_init : 0) |
                                (chunk + 1 == end_chunk ? marten_done : 0),
                                *chunk, strlen(*chunk));
  }
  
  if (test_data->error == 1) {
    return;
  }
  
  if (res != result) {
    fail("Invalid Result! Expected is %d but actual result is %d", result, res);
    return;
  }
  
  if (test_data->token < test_data->end_token) {
    fail("Underrun detected! Expected %lu tokens but actually produced %lu tokens", tokens, test_data->token - token);
    return;
  }
  
  if (test_data->token > test_data->end_token) {
    fail("Overrun detected! Expected %lu tokens but actually produced %lu tokens", tokens, test_data->token - token);
    return;
  }
  
  fprintf(stderr, "\r\e[1;32mPASS (:\e[0m\n");
}

#define test(title, result, chunks, ...) {                \
    const char *test_chunks[] = { chunks };               \
    const test_token_t test_tokens[] = { __VA_ARGS__ };   \
    run_test(title, result,                               \
             sizeof(test_chunks)/sizeof(test_chunks[0]),  \
             test_chunks,                                 \
             sizeof(test_tokens)/sizeof(test_tokens[0]),  \
             test_tokens);                                \
  }

#define show_type_size(type) printf("sizeof(" #type ") == %lu\n", sizeof(type))

int main(int argc, char *argv[]) {
  (void)argc;
  (void)argv;
  
  show_type_size(marten_response_parser_t);
  show_type_size(marten_head_parser_t);
  show_type_size(test_response_parser_t);
  
  test("Simple GET response", 0,
       chunk("HTTP/1.1 200 OK\r\n"
             "\r\n"),
       proto("HTTP", init field done)
       proto("1.1", init value done)
       status("200", init done)
       message("OK", init done));

  test("Simple GET response with body", 0,
       chunk("HTTP/1.1 200 OK\r\n"
             "\r\n"
             "Plain text"),
       proto("HTTP", init field done)
       proto("1.1", init value done)
       status("200", init done)
       message("OK", init done)
       body("Plain text", init done));

  test("Simple GET response with headers and body", 0,
       chunk("HTTP/1.1 200 OK\r\n"
             "Content-Type: text/plain\r\n"
             "Content-Length: 10\r\n"
             "\r\n"
             "Plain text"),
       proto("HTTP", init field done)
       proto("1.1", init value done)
       status("200", init done)
       message("OK", init done)
       head("Content-Type: text/plain\r\n"
            "Content-Length: 10\r\n", init done)
       head("Content-Type", init field done)
       head("text/plain", init value done)
       head("Content-Length", init field done)
       head("10", init value done)
       body("Plain text", init done));

  test("Chunked GET response with headers and body", 0,
       chunk("HTT")
       chunk("P/1.1")
       chunk(" 200 OK\r")
       chunk("\nContent-Type: text")
       chunk("/plain\r\nConten")
       chunk("t-Length: 10\r\n"
             "\r\n"
             "Plain tex")
       chunk("t"),
       proto("HTT", init field)
       proto("P", field done)
       proto("1.1", init value)
       proto("", value done)
       status("200", init done)
       message("OK", init done)
       head("Content-Type: text", init)
       head("Content-Type", init field done)
       head("text", init value)
       head("/plain\r\nConten")
       head("/plain", value done)
       head("Conten", init field)
       head("t-Length: 10\r\n", done)
       head("t-Length", field done)
       head("10", init value done)
       body("Plain tex", init)
       body("t", done));

  test("Real nginx response", 0,
       chunk("HTTP/1.1 304 Not Modified\r\n"
             "Server: nginx/1.6.2\r\n"
             "Date: Wed, 09 Nov 2016 14:56:01 GMT\r\n"
             "Last-Modified: Wed, 25 May 2011 20:43:55 GMT\r\n"
             "Connection: keep-alive\r\n"
             "ETag: \"4ddd6a0b-39d\"\r\n"
             "Expires: Fri, 09 Dec 2016 14:56:01 GMT\r\n"
             "Cache-Control: max-age=2592000\r\n"
             "\r\n"),
       proto("HTTP", init field done)
       proto("1.1", init value done)
       status("304", init done)
       message("Not Modified", init done)
       head("Server: nginx/1.6.2\r\n"
            "Date: Wed, 09 Nov 2016 14:56:01 GMT\r\n"
            "Last-Modified: Wed, 25 May 2011 20:43:55 GMT\r\n"
            "Connection: keep-alive\r\n"
            "ETag: \"4ddd6a0b-39d\"\r\n"
            "Expires: Fri, 09 Dec 2016 14:56:01 GMT\r\n"
            "Cache-Control: max-age=2592000\r\n", init done)
       head("Server", init field done)
       head("nginx/1.6.2", init value done)
       head("Date", init field done)
       head("Wed, 09 Nov 2016 14:56:01 GMT", init value done)
       head("Last-Modified", init field done)
       head("Wed, 25 May 2011 20:43:55 GMT", init value done)
       head("Connection", init field done)
       head("keep-alive", init value done)
       head("ETag", init field done)
       head("\"4ddd6a0b-39d\"", init value done)
       head("Expires", init field done)
       head("Fri, 09 Dec 2016 14:56:01 GMT", init value done)
       head("Cache-Control", init field done)
       head("max-age=2592000", init value done))
  
  return 0;
}
