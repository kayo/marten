#include "common.h"

int marten_router_handle(const marten_router_t *router,
                         marten_route_t *route,
                         marten_hash_t *hash,
                         marten_flags_t flags,
                         const char *ptr, size_t len) {
  if (marten_is(flags, marten_method) ||
      marten_is(flags, marten_path)) {
    /* reset route */
    if (marten_has(flags, marten_init) &&
        marten_is(flags, marten_method)) {
      *route = marten_route_none;
    }

    if (marten_is(flags, marten_path) &&
        marten_has(flags, marten_init)) {
      /* reset init flag to prevent hash reset */
      flags &= ~marten_init;
    }
    
    /* update hash */
    marten_hash_update(hash, flags, ptr, len);
    
    /* find route */
    if (marten_is(flags, marten_path) &&
        marten_has(flags, marten_done)) {
      *route = router->match(*hash);
      return unlikely(*route == marten_route_none) ? 1 : 0;
    }
  } else if (*route != marten_route_none) {
    /* redirect call to handler of route */
    return router->apply(route, flags, ptr, len);
  }
  
  return 0;
}
