Marten
======

Ultra-Lightweight HTTP Server/Client Construction Kit

Preface
-------

Development of **Marten** was started in purpose of creating an embedded HTTP server.

There is really tiny set of lightweight implementations of http parser.
One of this set is a *libhttp_parser*, which initially developed for **Nginx** http server.
In early stage of development I started from this but later it became clear, what *http_parser* not so good for embedding in software for mcu-based devices.

Later **Marten** moved away from the concept of server, because server usually means a framework.
But truly lightweight solution is possible only then, when developer decides yourself what he need in each concrete case.

Now the **Marten** is a Construction Kit for embedded developers.

Key Features
------------

* Asynchronous non-blocking design
* Working with data of variable length
* Refusal of handling corrupted data
* Explicit state and event-driven model
* Easy nesting/piping of handlers
* Using hashes instead of strings everywhere, where it is possible

Problems and Solutions
----------------------

### Request processing pipeline

The request handling of most http-servers is organized the following manner:

* Server accepts request
* Server receives, parses, and saves incoming data in memory
* Server calls some handler, when incoming data is ended
* The handler does something with received data from memory
* The handler initiates response, when internal processing is complete
* The server sends response to received request

Such manner of request processing is simple and provides insulation the protocol level and the application level.
But in same time, it have significant useless overhead, related to storing all data from request, regardless of that, what pieces of data is required for specific handler.

This problem also has another side. Usually the server doesn't know about the types of user-defined data, so it forced to store all data in that form, as it received. In any case the handler need to parse data yourself.

For example, suppose we have query parameters: `offset` and `length`.
In terms of application (i.e. handler), both parameters is `uint16_t`, but server generalises it to strings (null-terminated).
So the server needs to store two strings instead of two 16-bit unsigned numbers.
But this is not so bad, what really is so bad: the server needs store two strings, which contains the names of parameters.

If server is received the query `"?offset=123&length=45"`, then it need to store four C-strings: `"offset"` (7-bytes), `"123"` (4-bytes), `"length"` (7-bytes), `"45"` (3-bytes), total 21 bytes instead of only 4 bytes, in which our handler really needs.

But real requests has many data, which absolutely useless for our handles, but which HTTP recommends to send.
For example, there is a tipical Chromium request:

```
GET / HTTP/1.1
Host: illumium.org
Connection: keep-alive
Cache-Control: max-age=0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36
Accept-Encoding: gzip, deflate, sdch
Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
Cookie: sessionid=o12dsfa2ae2rAf4mbo9hfdsg; js=1

```

Suppose our handler interests in a following pieces of data:

* The method (`"GET"`), can be represented as value of enum type (at most 1 byte)
* The path (`"/"`), can be represented as pointer to specific handler (4 bytes for 32-bit archs)
* The value of *Accept-Encoding* header, can be represented as bitset (typical 1 byte)
* The value of *Accept-Language* header, can be represented as bitset (typical 1 byte)
* The authorization status flag related to *sessionid* field of *Cookie* header. (1 bit)

So we need store only 8-bytes, but traditional server needs to store up to 500 bytes for this simple request.

To solve this problem the **Marten** request processing is designed as pipeline of nested parsers.

### Design of parsers

Usually the embedded software uses buffers of fixed size to data exchange.
The size of buffers is dependent from low-level transfer protocol or hardware implementation and may varies in wide range.
In common case the buffer size may be variable.

The most of streamed HTTP parsers is designed to return only completely received entities and the number of processed bytes.
The applications needs to prepend unparsed data from previous step to the next chunk in order to correctly resume the parser.
Such design is unacceptable for embedded applications because it breeds many intractable problems.

The **Marten** http parser consumes any number of bytes (typically all of received at the moment of call) and it can be simply suspended/resumed in any point without tricks and workarounds.

In **Marten** handler callback gets target data chunk and some flags:

1. First we needs a markers of beginning and end of entity, because each result of parsing may consists of multiple parts.
2. Second we would like to have the identifiers for captured entities.
3. And last, it may be useful to have a flag, which indicates field or value, because some entities (for example http headers) may have both keys and values.

### Providing access to user-data

The data-types in Marten don't includes the user-data.
Actually you not need special fields to store pointers to your data instances.
Although most libraries uses this technique, the encapsulation is more universal method, which makes your data are available in any place of program.

Consider the following example:

```c
typedef struct {
  /* state-related fields */
  /* ... */
  /* pointer to user-data */
  void *data;
} parser_state_t;

typedef struct {
  /* state-related fields */
  /* ... */
  /* pointer to user-data */
  void *data;
} sender_state_t;

typedef struct {
  /* user-data fields */
  /* ... */
} user_data_t;

/* initialization */
parser_state_t *parser_state = new_parser_state();
sender_state_t *sender_state = new_sender_state();
user_data_t *user_data = new_user_state();

parser_state->data = user_data;
sender_state->data = user_data;

/* access to user-data using states */
user_data_t *user_data = (user_data_t*)parser_state->data;
user_data_t *user_data = (user_data_t*)sender_state->data;
```

If state structures has the pointer to user-data, then some overhead is inevitable.
Now consider the other technique of access to user-data:

```c
#define containerof(field_pointer, container_type, field_name) \
  ((container_type*)(((byte*)field_pointer) -                  \
    ((byte*)&((container_type*)NULL)->field_name)))

typedef struct {
  /* state-related fields */
  ...
} parser_state_t;

typedef struct {
  /* state-related fields */
  ...
} sender_state_t;

typedef struct {
  /* embed the parser state */
  parser_state_t parser_state;
  /* embed the sender state */
  sender_state_t sender_state;
  /* user-data fields */
  ...
} user_state_t; /* instead of user_data_t */

/* initialization */
user_state_t *user_state = new_user_state();

/* access to user-data using states */
/* transparent copy pointer */
user_state_t *user_state = containerof(parser_state, user_state_t, parser_state);
/* extra pointer arithmetic */
user_state_t *user_state = containerof(sender_state, user_state_t, sender_state);

```

We needs less memory in the second case because we don't need to store two pointers.
Also it required only single allocation to initialize several instances.

*To Be Continued*
