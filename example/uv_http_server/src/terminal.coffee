#
# Controller
#

#{request} = require "http"


@Ctrl = (Base)->
  class Terminal extends Base
    constructor: ->
      super
      @input = ""
      @output = ""

      @_evtSrc = new EventSource "/terminal/sse"
      @_evtSrc.onmessage = @recvOutput
      @_lastEventId = 0
    
    recvOutput: ({data})=>
      @output = "" if @_lastEventId > @_evtSrc.lastEventId
      @_lastEventId = @_evtSrc.lastEventId
      @output += data
      do @$render

    setInput: (@input)=>
      do @$render

    sendInput: =>
      @sending = on
      do @$render
      try
        ###
        request
          path: "/terminal"
          method: "PUT"
        , ({statusCode})=>
          @sending = no
          if statusCode is 204
            @input = ""
            @error = no
          else
            @error = yes
          do @$render
        .end @input + "\n"
        ###
        req = new XMLHttpRequest
        req.open "PUT", "/terminal", yes
        req.setRequestHeader "Content-Type", "text/plain"
        req.send @input + "\n"
        req.onreadystatechange = =>
          if req.readyState is 4
            req.onreadystatechange = null
            @sending = off
            if req.status is 204
              @input = ""
              @error = no
            else
              @error = yes
            do @$render
      catch e
        @sending = no
        @error = yes
        do @$render

#
# View
#

{pre} = require "./node"
{Form, Group, Entry, Select, Button} = require "./form"

outputLabel =
  en: "Output"
  ru: "Вывод"

inputLabel =
  en: "Input"
  ru: "Ввод"

Terminal = ({language, input, output, setInput, sendInput, sending})->
  Form
    aligned: yes
    Group
      label: outputLabel[language]
      pre null,
        output
      Entry
        id: "name"
        label: inputLabel[language]
        change: setInput
        value: input
        keydown: ({keyCode})-> do sendInput if keyCode is 13
        disabled: sending

@view =
  view: Terminal
  title:
    en: "Terminal"
    ru: "Терминал"
