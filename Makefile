# Compiler
#COMPILER_NAME ?= arm-none-eabi-
#OBJECT_ARCH ?= elf32-littlearm arm
#GDBREMOTE ?= localhost:3333

# Base path to build root
BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

MKDIR := $(BASEPATH)rules
INCDIR := $(BASEPATH)include
SRCDIR := $(BASEPATH)src
LIBSDIR := $(BASEPATH)libs

-include config.mk

include $(MKDIR)/macro.mk
include $(MKDIR)/build.mk
include $(MKDIR)/stalin.mk
include $(MKDIR)/pkgconfig.mk
include $(MKDIR)/option.mk

include $(BASEPATH)marten.mk

$(call use_toolchain,stalin)

$(call ADDRULES,\
OPT_RULES:TARGET.OPTS)

include $(BASEPATH)martenpp.mk

ifneq (,$(call option-true,$(marten.test)))
include $(BASEPATH)test/test.mk
endif

ifneq (,$(call option-true,$(marten.example)))
include $(BASEPATH)example/example.mk
endif

# Provide rules
$(call ADDRULES,\
PKG_RULES:TARGET.PKGS\
LIB_RULES:TARGET.LIBS\
DYN_RULES:TARGET.DYNS\
BIN_RULES:TARGET.BINS)

$(eval $(call MARTENPP_RULE))
