VNode = require "snabbdom/vnode"
{array, primitive} = require "snabbdom/is"

###
  Virtual DOM Node
  
  @param [String] sel The node tag or selector
  @param [Object] data The node data (props, attrs, events, etc.)
  @param [Array] children The children nodes of node
  @param [String|Number] text The text entry of node
  @param [DOMNode???] elm The real DOM element???
###

none = undefined

textnode = ({sel, text})->
  not sel? and text?

TNode = (tag)-> (data, children...)->
  data ?= {} # force the data to be an object
  index = 0
  while index < children.length
    child = children[index]
    unless child? # drop empty child
      children.splice index, 1
      continue # restart from the same index
    if array child # unzip array children
      #children = [children[...index]..., child..., children[index+1...]...]
      children.splice index, 1, child... # use mutating instead
      continue # restart from same index
    if primitive child # wrap primitive to VNode
      if index > 0 and textnode children[index-1] # append to previous text node
        children[index-1].text += child
        children.splice index, 1
        continue # restart from same index
      else # create new text node
        children[index] = VNode none, none, none, child # mutate original value
    index++
  if children.length is 0 # freeing empty children
    children = none
  else if children.length is 1 and textnode children[0] # use single text node as text
    {text} = children[0]
    children = none
  # create node
  VNode tag, data, children, text

@[tag] = TNode tag for tag in "a,abbr,address,area,article,aside,audio,b,base,bdi,bdo,blockquote,body,br,button,canvas,caption,cite,code,col,colgroup,command,datalist,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,head,header,hgroup,hr,html,i,iframe,img,input,ins,kbd,keygen,label,legend,li,link,map,mark,menu,meta,meter,nav,noscript,object,ol,optgroup,option,output,p,param,pre,progress,q,rp,rt,ruby,s,samp,script,section,select,small,source,span,strong,style,sub,summary,sup,table,tbody,td,textarea,tfoot,th,thead,time,title,tr,track,u,ul,var,video,wbr".split /,/
