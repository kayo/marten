#include "common.h"
#include "parser.h"

#define states(state)        \
  state(recv_field_seed)     \
  state(recv_field)          \
  state(recv_field_pct0)     \
  state(recv_field_pct1)     \
  state(recv_value_seed)     \
  state(recv_value)          \
  state(recv_value_pct0)     \
  state(recv_value_pct1)

#define flushes(state, state_range)            \
  state_range(recv_field, recv_field_pct1,     \
              marten_field)                    \
  state_range(recv_value, recv_value_pct1,     \
              marten_value)

enum {
  _marten_recv_done = 0,
#define state(name) _marten_##name,
  states(state)
#undef state
};

/* parser runtime state */
typedef struct {
  marten_handler_t *handler;
  marten_request_parser_t *state;
  const char *ini;
  const char *ptr;
  const char *end;
  const char *tmp;
  int res;
  marten_flags_t ent;
} _marten_spr_t;

static void _marten_emit(_marten_spr_t *sprp) {
  const char *ptr = sprp->tmp ? sprp->tmp : sprp->ini;
  size_t len = sprp->ptr - ptr;
  marten_flags_t flags = marten_none;
  
  switch (*sprp->state) {
#define state(name, _flags) case _marten_##name: flags = _flags; break;
#define state_range(name1, name2, _flags) case _marten_##name1 ... _marten_##name2: flags = _flags; break;
    flushes(state, state_range);
#undef state
#undef state_range
  }
  
  if (flags) {
    flags |= sprp->ent;
    sprp->res = sprp->handler(sprp->state,
                              (sprp->tmp ? marten_init : 0) |
                              (sprp->ptr == sprp->end ? 0 : marten_done) |
                              flags, ptr, len);
  }
  
  if (sprp->tmp) {
    sprp->tmp = NULL;
  }
}

chunk(recv_field_seed) {
  if (unlikely(ptr_char == '%')) {
    turn(recv_field_pct0);
    ent_init();
  } else if (likely(isqsc(ptr_char))) {
    turn(recv_field);
    ent_init();
  } else if (unlikely(ptr_char == SP)) {
    turn(recv_done);
  } else {
    fail();
  }
}

chunk(recv_field) {
  if (unlikely(ptr_char == '&')) {
    ent_done();
    turn(recv_field_seed);
  } else if (unlikely(ptr_char == '=')) {
    ent_done();
    turn(recv_value_seed);
  } else if (likely(isqsc(ptr_char))) {
    ent_char();
  } else if (unlikely(ptr_char == SP)) {
    ent_done();
    turn(recv_done);
  } else if (unlikely(ptr_char == '%')) {
    turn(recv_field_pct0);
  } else {
    fail();
  }
}

chunk(recv_field_pct0) {
  if (likely(ishex(ptr_char))) {
    ent_char();
    turn(recv_field_pct1);
  } else {
    fail();
  }
}

chunk(recv_field_pct1) {
  if (likely(ishex(ptr_char))) {
    ent_char();
    turn(recv_field);
  } else {
    fail();
  }
}

chunk(recv_value_seed) {
  if (unlikely(ptr_char == '&')) {
    turn(recv_field_seed);
  } else if (likely(isqsc(ptr_char))) {
    turn(recv_value);
    ent_init();
  } else if (unlikely(ptr_char == SP)) {
    ent_done();
    turn(recv_done);
  } else if (unlikely(ptr_char == '%')) {
    turn(recv_value_pct0);
    ent_init();
  } else {
    fail();
  }
}

chunk(recv_value) {
  if (unlikely(ptr_char == '&')) {
    ent_done();
    turn(recv_field_seed);
  } else if (likely(isqsc(ptr_char))) {
    ent_char();
  } else if (unlikely(ptr_char == SP)) {
    ent_done();
    turn(recv_done);
  } else if (unlikely(ptr_char == '%')) {
    ent_char();
    turn(recv_value_pct0);
  } else {
    fail();
  }
}

chunk(recv_value_pct0) {
  if (likely(ishex(ptr_char))) {
    ent_char();
    turn(recv_value_pct1);
  } else {
    fail();
  }
}

chunk(recv_value_pct1) {
  if (likely(ishex(ptr_char))) {
    ent_char();
    turn(recv_value);
  } else {
    fail();
  }
}

int marten_query_parse(marten_request_parser_t *state,
                       marten_handler_t *handler,
                       marten_flags_t flags,
                       const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    *state = _marten_recv_field_seed;
  }

  if (ptr == NULL) {
    return 0;
  }
  
  _marten_spr_t spr = {
    handler,
    state,
    ptr,
    ptr,
    ptr + len,
    NULL,
    0,
    flags & marten_entity
  };
  
  for (; spr.ptr < spr.end && *state != _marten_recv_done && spr.res == 0; spr.ptr++) {
    switch (*state) {
#define state(name) case _marten_##name: _marten_##name##_chunk(&spr); break;
      states(state);
#undef state
    default: spr.res = 1;
    }
  }
  
  if (spr.res == 0) {
    if (marten_has(flags, marten_done)) {
      spr.end++;
    }
    
    _marten_emit(&spr);
    
    if (marten_has(flags, marten_done)) {
      *state = _marten_recv_done;
    }
  }
  
  return spr.res;
}

enum {
  _marten_query_pct1_mask = 0x0f,
  
  _marten_query_pct0 = 1 << 4,
  _marten_query_pct1 = 1 << 4,
};

int marten_query_decode(marten_query_decoder_t *state,
                        marten_handler_t *handler,
                        marten_flags_t flags,
                        const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    *state = 0;
  }
  
  const char *end = ptr + len;
  const char *tmp = ptr;
  int res = 0;
  
  for (; ptr < end && res == 0; ptr++) {
    if (unlikely(*state & _marten_query_pct0)) {
      *state = _marten_query_pct1 | hex2i(*ptr);
    } else if (unlikely(*state & _marten_query_pct1)) {
      *state = (*state & _marten_query_pct1_mask) | (hex2i(*ptr) << 4);
      res = handler(state, 0, (const char*)state, 1);
      *state = 0;
      tmp = ptr + 1;
    } else if (unlikely(*ptr == '%')) {
      res = handler(state, 0, tmp, ptr - tmp);
      *state = _marten_query_pct0;
    }
  }
  
  return res;
}
