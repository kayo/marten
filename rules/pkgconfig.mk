define PKG_RULES
$(1).CFLAGS += $$(shell pkg-config --cflags $(1))
$(1).LDFLAGS += $$(shell pkg-config --libs $(1))
endef
