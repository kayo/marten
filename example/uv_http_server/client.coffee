require "es5-shim"
require "es5-shim/es5-sham"
require "./shim/xhr.activex"
require "./shim/xhr.binary"
require "animation-frame"
.shim frameRate: 60
require "eventsource-polyfill"

require "./client.css"

{
  document
  requestAnimationFrame
} = window

{init} = require "snabbdom"
patch = init [ # Init patch function with choosen modules
  require "snabbdom/modules/class" # makes it easy to toggle classes
  require "snabbdom/modules/props" # for setting properties on DOM elements
  require "snabbdom/modules/attributes" # for setting attributes on DOM elements
  require "snabbdom/modules/style" # handles styling on elements with support for animations
  require "snabbdom/modules/eventlisteners" # attaches event listeners
]

{Main: Base, Root} = require "./src/main"

class Main extends Base
  constructor: (@Root)->
    @_pendRender = null
    @_needRender = no
    super
    do @$doTree
  
  $doTree: ->
    _domTree = @_domTree
    @_domTree = @Root @
    _domTree

  $mount: (@_rootNode)->
    patch @_rootNode, @_domTree

  $renderFrame: =>
    @_pendRender = null

    _domTree = do @$doTree
    patch _domTree, @_domTree
    
    if @_needRender
      @_needRender = no
      do @$forceRender

  $forceRender: ->
    @_pendRender = requestAnimationFrame @$renderFrame

  $render: ->
    return if @_needRender
    if @_pendRender?
      @_needRender = yes
    else
      do @$forceRender

boot = ->
  main = new Main Root
  main.$mount document.body

document.addEventListener (switch TARGET
  when "cordova" then "deviceready"
  else "DOMContentLoaded"), boot, no
