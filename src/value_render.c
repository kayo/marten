#include "common.h"
#include "parser.h"
#include "types.h"

char *marten_uint_tostr(char *buf, marten_uint_t val) {  
  char *p = buf;
  char t;
  
  do {
    *p++ = i2dec(val % 10);
    val /= 10;
  } while (val > 0);

  char *q = buf;
  char *r = p--;
  
  while (q < p) { /* reverse digits */
    t = *q;
    *q++ = *p;
    *p-- = t;
  }
  
  return r;
}

char *marten_uint_hex_tostr(char *buf, marten_uint_t val) {
  char *p = buf;
  char t;
  
  do {
    *p++ = i2hex(val % 16);
    val /= 16;
  } while (val > 0);

  char *q = buf;
  char *r = p--;
  
  while (q < p) { /* reverse digits */
    t = *q;
    *q++ = *p;
    *p-- = t;
  }
  
  return r;
}

char *marten_sint_tostr(char *buf, marten_sint_t val) {
  char *ptr = buf;

  if (val < 0) {
    *ptr++ = '-';
    val = -val;
  }
  
  return marten_uint_tostr(ptr, val);
}
