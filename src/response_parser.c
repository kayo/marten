#include "common.h"
#include "parser.h"

#define states(state)              \
  state(recv_proto_seed)           \
  state(recv_proto)                \
  state(recv_version_seed)         \
  state(recv_version)              \
  state(recv_status_seed)          \
  state(recv_status)               \
  state(recv_message_seed)         \
  state(recv_message)              \
  state(recv_head_seed_cr)         \
  state(recv_head_seed_lf)         \
  state(recv_head_seed)            \
  state(recv_head_feed_cr)         \
  state(recv_head_feed_lf)         \
  state(recv_head_feed)            \
  state(recv_head)                 \
  state(recv_body_seed_lf)         \
  state(recv_body_seed)            \
  state(recv_body)

#define flushes(state, state_range)                      \
  state(recv_proto, marten_proto | marten_field)         \
  state(recv_version, marten_proto | marten_value)       \
  state(recv_status, marten_status)                      \
  state(recv_message, marten_message)                    \
  state_range(recv_head_feed_cr, recv_head, marten_head) \
  state(recv_body, marten_body)

enum {
  _marten_recv_done = 0,
#define state(name) _marten_##name,
  states(state)
#undef state
};

/* parser runtime state */
typedef struct {
  marten_handler_t *handler;
  marten_request_parser_t *state;
  const char *ini;
  const char *ptr;
  const char *end;
  const char *tmp;
  int res;
} _marten_spr_t;

static void _marten_emit(_marten_spr_t *sprp) {
  const char *ptr = sprp->tmp ? sprp->tmp : sprp->ini;
  size_t len = sprp->ptr - ptr;
  marten_flags_t flags = marten_none;
  
  switch (*sprp->state) {
#define state(name, _flags) case _marten_##name: flags = _flags; break;
#define state_range(name1, name2, _flags) case _marten_##name1 ... _marten_##name2: flags = _flags; break;
    flushes(state, state_range);
#undef state
#undef state_range
  }
  
  if (flags) {
    sprp->res = sprp->handler(sprp->state,
                              (sprp->tmp ? marten_init : 0) |
                              (sprp->ptr == sprp->end ? 0 : marten_done) |
                              flags, ptr, len);
  }
  
  if (sprp->tmp) {
    sprp->tmp = NULL;
  }
}

chunk(recv_proto_seed) {
  if (likely(istok(ptr_char))) {
    turn(recv_proto);
    ent_init();
  } else {
    fail();
  }
}

chunk(recv_proto) {
  if (likely(istok(ptr_char))) {
    ent_char();
  } else if (ptr_char == '/') {
    ent_done();
    turn(recv_version_seed);
  } else {
    fail();
  }
}

chunk(recv_version_seed) {
  if (likely(isdec(ptr_char))) {
    turn(recv_version);
    ent_init();
  } else {
    fail();
  }
}

chunk(recv_version) {
  if (likely(isdec(ptr_char) || ptr_char == '.')) {
    ent_char();
  } else if (ptr_char == SP) {
    ent_done();
    turn(recv_status_seed);
  } else {
    fail();
  }
}

chunk(recv_status_seed) {
  if (likely(isdec(ptr_char))) {
    turn(recv_status);
    ent_init();
  } else {
    fail();
  }
}

chunk(recv_status) {
  if (likely(isdec(ptr_char))) {
    ent_char();
  } else if (ptr_char == SP) {
    ent_done();
    turn(recv_message_seed);
  } else {
    fail();
  }
}

chunk(recv_message_seed) {
  if (likely(istok(ptr_char))) {
    turn(recv_message);
    ent_init();
  } else {
    fail();
  }
}

chunk(recv_message) {
  if (likely(istok(ptr_char) || ptr_char == SP)) {
    ent_char();
  } else if (ptr_char == '\r') {
    ent_done();
    turn(recv_head_seed_lf);
  } else {
    fail();
  }
}

chunk(recv_head_seed_cr) {
  if (likely(ptr_char == '\r')) {
    turn(recv_head_seed_lf);
  } else {
    fail();
  }
}

chunk(recv_head_seed_lf) {
  if (likely(ptr_char == '\n')) {
    turn(recv_head_seed);
  } else {
    fail();
  }
}

chunk(recv_head_seed) {
  if (unlikely(ptr_char == '\r')) {
    turn(recv_body_seed_lf);
  } else if (likely(istok(ptr_char))) {
    turn(recv_head);
    ent_init();
  } else {
    fail();
  }
}

chunk(recv_head) {
  if (unlikely(ptr_char == '\r')) {
    turn(recv_head_feed_lf);
  } else {
    ent_char();
  }
}

chunk(recv_head_feed_cr) {
  if (likely(ptr_char == '\r')) {
    turn(recv_head_feed_lf);
  } else {
    fail();
  }
}

chunk(recv_head_feed_lf) {
  if (likely(ptr_char == '\n')) {
    turn(recv_head_feed);
  } else {
    fail();
  }
}

chunk(recv_head_feed) {
  if (unlikely(ptr_char == '\r')) {
    ent_done();
    turn(recv_body_seed_lf);
  } else {
    ent_char();
    turn(recv_head);
  }
}

chunk(recv_body_seed_lf) {
  if (likely(ptr_char == '\n')) {
    turn(recv_body_seed);
  } else {
    fail();
  }
}

chunk(recv_body_seed) {
  turn(recv_body);
  ent_init();
}

chunk(recv_body) {
  (void)sprp;
  ent_char();
}

int marten_response_parse(marten_response_parser_t *state,
                          marten_handler_t *handler,
                          marten_flags_t flags,
                          const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    *state = _marten_recv_proto_seed;
  }
  
  if (ptr == NULL) {
    return 0;
  }
  
  _marten_spr_t spr = {
    handler,
    state,
    ptr,
    ptr,
    ptr + len,
    NULL,
    0
  };
  
  for (; spr.ptr < spr.end && *state != _marten_recv_done && spr.res == 0; spr.ptr++) {
    switch (*state) {
#define state(name) case _marten_##name: _marten_##name##_chunk(&spr); break;
      states(state);
#undef state
    default: spr.res = 1;
    }
  }
  
  if (spr.res == 0) {
    if (marten_has(flags, marten_done)) {
      spr.end++;
    }
    
    _marten_emit(&spr);
    
    if (marten_has(flags, marten_done)) {
      *state = _marten_recv_done;
    }
  }
  
  return spr.res;
}

int marten_response_limit(marten_limiter_t *offset,
                          int32_t header_limit,
                          int32_t message_limit,
                          marten_response_parser_t *state,
                          marten_handler_t *handler,
                          marten_flags_t flags,
                          const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    *offset = 0; /* initialize octet counter */
  }
  
  int32_t limit = likely(*state >= _marten_recv_body) ? message_limit : header_limit;
  
  if (limit >= 0 && *offset + len > (uint32_t)limit) {
    return 1;
  }
  
  int res = handler(offset, flags, ptr, len);
  
  if (res == 0) {
    *offset += len;
  }
  
  return res;
}
