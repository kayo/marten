martenpp.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libmartenpp
libmartenpp.INHERIT := stalin
libmartenpp.CDIRS := $(martenpp.BASEPATH)include
libmartenpp.SRCS := $(addprefix $(martenpp.BASEPATH),\
  src/string_hash.c \
  tools/martenpp.c)

TARGET.OPTS += libmartenpp
libmartenpp.OPTS := $(martenpp.BASEPATH)src/marten.cf

TARGET.BINS += martenpp
martenpp.INHERIT := stalin
martenpp.DEPLIBS* := libmartenpp

define MARTENPP_RULE # <library> <c|S> <orig-source> <source> <destination> <source-args> <rest-rules>
$(1).TMP += $(call GEN_P,$(1),$(3),m)
$(call GEN_P,$(1),$(3),m): $(4)
	@echo TARGET $(1) MARTENPP $(2) $$< $(5)
	$(Q)$$(martenpp.BIN) -o $$@ $$<
$(call NEXT_RULE,$(1),$(2),$(3),$(call GEN_P,$(1),$(3),m),$(5),$(6))
.SECONDEXPANSION:
$(call GEN_P,$(1),$(3),m): $$$$(martenpp.BIN)
endef

marten-pp.CCPIPE.c = $(if $(call option-true,$(marten.use_pp)),CPP_RULE MARTENPP_RULE OBJ_RULE)
