libmarten.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libmarten
libmarten.INHERIT := stalin
libmarten.CDIRS := $(libmarten.BASEPATH)include
libmarten.SRCS := $(addprefix $(libmarten.BASEPATH)src/,\
  string_hash.c \
  static_pool.c \
  request_parser.c \
  response_parser.c \
  head_parser.c \
  query_parser.c \
  value_parser.c \
  path_router.c \
  value_render.c \
)

TARGET.OPTS += libmarten
libmarten.OPTS := $(libmarten.BASEPATH)src/marten.cf

TARGET.DYNS += $(if $(option-true,$(marten.shared)),libmarten)
libmarten-shared.NAME = libmarten
libmarten-shared.DYNVER = $(marten.version)
libmarten-shared.DEPLIBS* := libmarten
