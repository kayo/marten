#include "common.h"
#include "parser.h"

static force_inline void
_marten_hash_init(marten_hash_t *hash) {
  *hash = MARTEN_HASH_CALL(init);
}

static force_inline void
_marten_hash_byte(marten_hash_t *hash, uint8_t val) {
  *hash = MARTEN_HASH_CALL(hash)(*hash, val);
}

marten_hash_t marten_hash_str(const char *s) {
  marten_hash_t hash;
  _marten_hash_init(&hash);
  for (; *s != '\0'; )
    _marten_hash_byte(&hash, *s++);
  return hash;
}

void marten_hash_update(marten_hash_t *hash,
                        marten_flags_t flags,
                        const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    /* Hashing initialization on beginning of string */
    _marten_hash_init(hash);
  }
  
  if (ptr == NULL) {
    return;
  }
  
  const char *end = ptr + len;

  if (marten_is(flags, marten_head)) {
    /* Case-independent hashing for header strings */
    for (; ptr < end; )
      _marten_hash_byte(hash, lower(*ptr++));
  } else {
    for (; ptr < end; )
      _marten_hash_byte(hash, *ptr++);
  }
}
