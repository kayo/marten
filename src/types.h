#ifndef MARTEN_UINT_TYPES
#define MARTEN_UINT_TYPES(T) T(uint8_t) T(uint16_t) T(uint32_t) T(uint64_t)
#endif

#ifndef MARTEN_SINT_TYPES
#define MARTEN_SINT_TYPES(T) T(int8_t) T(int16_t) T(int32_t) T(int64_t)
#endif
