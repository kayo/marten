#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <marten.h>

static int usage(const char *program) {
  fprintf(stderr, "Usage: %s [-o <output_file>] <input_file>\n", program);
  return EXIT_FAILURE;
}

enum {
  seek_macro_name,
  seek_open_paren,
  seek_begin_string,
  read_string_data,
  skip_escape_char,
  seek_oct_char1,
  seek_oct_char2,
  seek_hex_char1,
  seek_hex_char2,
  seek_close_paren,
};

static inline int isodigit(int c) {
  return '0' <= c && c <= '7';
}

static inline int fromhex(int c) {
  return c <= '9' ? c - '0' : (c <= 'Z' ? c - 'A' : c <= 'z' ? c - 'a' : -11) + 10;
}

static int process(FILE *input, FILE *output) {
  int s = seek_macro_name;
  int c;
  int n = 0;
  marten_hash_t h = MARTEN_HASH_CALL(init);
  
  static const char *marten_hash_cstr = "marten_hash_cstr";
  const char *macro_name_ptr = marten_hash_cstr;
  
  for (; (c = fgetc(input)) != EOF; ) {
    switch (s) {
    case seek_macro_name:
      if (c == *macro_name_ptr) {
        macro_name_ptr++;
        if (*macro_name_ptr == '\0') {
          s = seek_open_paren;
          macro_name_ptr = marten_hash_cstr;
        }
      } else {
        const char *ptr = marten_hash_cstr;
        for (; ptr < macro_name_ptr; ) {
          fputc(*ptr++, output);
        }
        fputc(c, output);
        macro_name_ptr = marten_hash_cstr;
      }
      break;
    case seek_open_paren:
      if (c == '(') {
        s = seek_begin_string;
      } else if (!isspace(c)) {
        fputs(marten_hash_cstr, output);
        fputc(c, output);
        s = seek_macro_name;
      }
      break;
    case seek_close_paren:
      if (c == ')') {
        fprintf(output, "%uU", h);
        s = seek_macro_name;
      } else if (c == '"') {
        s = read_string_data;
      } else if (!isspace(c)) {
        /* error */
      }
      break;
    case seek_begin_string:
      if (c == '"') {
        s = read_string_data;
        h = MARTEN_HASH_CALL(init);
      } else if (!isspace(c)) {
        fputs(marten_hash_cstr, output);
        fputc('(', output);
        fputc(c, output);
        s = seek_macro_name;
      }
      break;
    case read_string_data:
      if (c == '\\') {
        s = skip_escape_char;
      } else if (c != '"') {
        h = MARTEN_HASH_CALL(hash)(h, c);
      } else {
        s = seek_close_paren;
      }
      break;
    case skip_escape_char:
      switch (c) {
      case 'a': c = '\a'; break;
      case 'b': c = '\b'; break;
      case 'f': c = '\f'; break;
      case 'r': c = '\r'; break;
      case 'n': c = '\n'; break;
      case 't': c = '\t'; break;
      case 'v': c = '\v'; break;
      case '\\': c = '\\'; break;
      case '\'': c = '\''; break;
      case '"': c = '"'; break;
      case '?': c = '\?'; break;
      case 'x':
        s = seek_hex_char1;
        break;
      default:
        if (isodigit(c)) {
          s = seek_oct_char1;
          n = fromhex(c);
        }
      }
      if (s == skip_escape_char) {
        h = MARTEN_HASH_CALL(hash)(h, c);
        s = read_string_data;
      }
      break;
    case seek_oct_char1:
      if (isodigit(c)) {
        n |= fromhex(c) << 3;
        s = seek_oct_char2;
      } else {
        h = MARTEN_HASH_CALL(hash)(h, c);
        s = read_string_data;
      }
      break;
    case seek_oct_char2:
      if (isodigit(c)) {
        n |= fromhex(c) << 6;
        h = MARTEN_HASH_CALL(hash)(h, n);
      } else {
        h = MARTEN_HASH_CALL(hash)(h, c);
      }
      s = read_string_data;
      break;
    case seek_hex_char1:
      if (isxdigit(c)) {
        n = fromhex(c);
        s = seek_oct_char2;
      } else {
        h = MARTEN_HASH_CALL(hash)(h, c);
        s = read_string_data;
      }
      break;
    case seek_hex_char2:
      if (isxdigit(c)) {
        n |= fromhex(c) << 4;
        h = MARTEN_HASH_CALL(hash)(h, n);
      } else {
        h = MARTEN_HASH_CALL(hash)(h, c);
      }
      s = read_string_data;
      break;
    }
  }
  
  return EXIT_SUCCESS;
}

int main(int argc, char *const argv[]) {
  const char *infile = NULL;
  const char *outfile = NULL;
  int optres;
  
  for (; (optres = getopt(argc, argv, "o:vh?")) != -1; ) {
    switch (optres) {
    case 'o':
      outfile = optarg;
      break;
    case 'h':
    default: /* '?' */
      return usage(argv[0]);
    }
  }
  
  if (argc - optind > 1) {
    return usage(argv[0]);
  }
  
  if (optind < argc) {
    infile = argv[optind];
  }
  
  if (infile != NULL && outfile != NULL && 0 == strcmp(infile, outfile)) {
    fprintf(stderr, "%s: error: %s: Output file is same as input\n", argv[0], infile);
    return EXIT_FAILURE;
  }
  
  FILE *input = infile ? fopen(infile, "r") : stdin;
  
  if (!input) {
    fprintf(stderr, "%s: error: %s: File not found\n", argv[0], infile);
    return EXIT_FAILURE;
  }
  
  FILE *output = outfile ? fopen(outfile, "w") : stdout;
  
  if (!output) {
    fprintf(stderr, "%s: error: %s: Can not create file\n", argv[0], outfile);
    return EXIT_FAILURE;
  }
  
  return process(input, output);
}
