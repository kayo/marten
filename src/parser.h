#define SP ' '
#define HT '\t'
#define CR '\r'
#define LF '\n'

/**
 * @brief Check for control character
 */
static force_inline char isctl(char c) {
  return c <= 31 || c == 127;
}

/**
 * @brief Check for HTTP separator character
 */
static force_inline char issep(char c) {
  return c == '(' || c == ')' || c == '<' || c == '>' || c == '@'
    || c == ',' || c == ';' || c == ':' || c == '\\'
    || c == '/' || c == '[' || c == ']' || c == '?' || c == '='
    || c == '{' || c == '}' || c == SP || c == HT;
}

/**
 * @brief Check for HTTP token character
 */
static force_inline char istok(char c) {
  return !isctl(c) && !issep(c);
}

/*
  path-abempty  = *( "/" segment )

  segment       = *pchar
  query       = *( pchar / "/" / "?" )
  
  pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"

  pct-encoded = "%" HEXDIG HEXDIG

  reserved    = gen-delims / sub-delims
  gen-delims  = ":" / "/" / "?" / "#" / "[" / "]" / "@"
  sub-delims  = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
  unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"
*/

/**
 * @brief Check for latin characters in both cases
 */
static force_inline char islat(char c) {
  return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
}

/**
 * @brief Check for decimal integers
 */
static force_inline char isdec(char c) {
  return '0' <= c && c <= '9';
}

/**
 * @brief Check for unreserved characters
 */
static force_inline char isunr(char c) {
  return islat(c) || isdec(c) || c == '-' || c == '.' || c == '_' || c == '~';
}

/**
 * @brief Check for sub-delims characters
 */
static force_inline char issub(char c) {
  return c == '!' || c == '$' || ('&' <= c && c <= ',') || c == ';' || c == '=';
}

/**
 * @brief Check for path segment
 */
static force_inline char isseg(char c) {
  return isunr(c) || issub(c) || c == ':' || c == '@';
}

/**
 * @brief Check for query string characters
 */
static force_inline char isqsc(char c) {
  return isseg(c) || c == '/' || c == '?';
}

/**
 * @brief Check for hexademical number
 */
static force_inline char ishex(char c) {
  return isdec(c) || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
}

/**
 * @brief Convert hexademical char to integer
 */
static force_inline char hex2i(char c) {
  return c - (c <= '9' ? '0' : (c <= 'Z' ? 'A' : 'a') - 10);
}

static const char hex[] = "0123456789abcdef";

/**
 * @brief Convert integer to hexademical char
 */
static force_inline char i2hex(char h) {
  return h < 16 ? hex[(size_t)h] : -1;
}

/**
 * @brief Convert decimal char to integer
 */
static force_inline char dec2i(char c) {
  return c - '0';
}

/**
 * @brief Convert integer to decimal char
 */
static force_inline char i2dec(char h) {
  return h < 10 ? hex[(size_t)h] : -1;
}

/**
 * @brief Converts to lower case
 */
static force_inline char lower(char c) {
  return unlikely('A' <= c && c <= 'Z') ? c - 'A' + 'a' : c;
}

#define ptr_char (*sprp->ptr)
#define ptr_init() (sprp->ptr == sprp->ini)
#define ptr_done() (sprp->ptr == sprp->end)

#define hash_init() debug("hash !"); _marten_hash_init(&sprp->conn->ent_hash)
#define hash_byte(val) debug("hash #%c", val); _marten_hash_byte(&sprp->conn->ent_hash, val)

#define ent_init() debug("ent ^"); (sprp->tmp = sprp->ptr); ent_char()
#define ent_char() debug("ent [%c]", ptr_char)
#define ent_done() debug("ent $"); { \
    _marten_emit(sprp);              \
    if (sprp->res != 0) {            \
      debug("ent *");                \
      return;                        \
    } }

#define turn(name) debug("state -> " #name); *sprp->state = _marten_##name
#define fail() debug("fail"); sprp->res = 1
#define chunk(name) static force_inline void _marten_##name##_chunk(_marten_spr_t *sprp)
