#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <marten.h>

#include "debug.h"

typedef struct {
  int fd;
  marten_request_parser_t request;
  marten_response_writer_t response;
  marten_hash_t hash;
} sconn_t;

static marten_define_writer(root) {
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_cstr_write(writer, "Content-Type: text/plain\r\nContent-Length: 9\r\n\r\nRoot Page");
  marten_end(writer);
}

static marten_define_writer(pong) {
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 200, "OK");
  marten_cstr_write(writer, "Content-Type: text/plain\r\nContent-Length: 4\r\n\r\npong");
  marten_end(writer);
}

static marten_define_writer(notfound) {
  marten_begin(writer);
  marten_status_cstr_write(writer, "HTTP/1.1", 404, "Not Found");
  marten_cstr_write(writer, "Content-Type: text/plain\r\nContent-Length: 18\r\n\r\nResource Not Found");
  marten_end(writer);
}

static marten_define_handler(on_parse) {
  sconn_t *conn = marten_containerof(state, sconn_t, request);
  
  if (marten_is(flags, marten_method) ||
      marten_is(flags, marten_path)) {
    marten_hash_update(&conn->hash, flags, ptr, len);
  }
  
  if (marten_has(flags, marten_done)) {
    if (marten_is(flags, marten_method) &&
        conn->hash != marten_hash("GET")) {
      return 1;
    }
    if (marten_is(flags, marten_path)) {
      if (conn->hash == marten_hash("/")) {
        marten_respond(&conn->response, root);
      } else if (conn->hash == marten_hash("/ping")) {
        marten_respond(&conn->response, pong);
      } else {
        marten_respond(&conn->response, notfound);
      }
      return 1;
    }
  }

  return 0;
}

static marten_define_handler(do_write) {
  (void)flags;
  
  sconn_t *conn = marten_containerof(state, sconn_t, response);
  
  rawlog("send", ptr, len);
  
  write(conn->fd, ptr, len);
  
  return 0;
}

int main(void) {
  struct sockaddr_in sa;
  int sock_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  
  if (-1 == sock_fd) {
    error("cannot create socket");
    exit(EXIT_FAILURE);
  }
  
  int on = 1;
  if (-1 == setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))) {
    error("setsockopt failed");
  }
  
  memset(&sa, 0, sizeof sa);
  
  sa.sin_family = AF_INET;
  sa.sin_port = htons(8000);
  sa.sin_addr.s_addr = htonl(INADDR_ANY);
  
  if (-1 == bind(sock_fd,(struct sockaddr *)&sa, sizeof sa)) {
    error("bind failed");
    close(sock_fd);
    exit(EXIT_FAILURE);
  }
  
  if (-1 == listen(sock_fd, 128)) {
    error("listen failed");
    close(sock_fd);
    exit(EXIT_FAILURE);
  }
  
  for (;;) {
    sconn_t conn;
    
    conn.fd = accept(sock_fd, NULL, NULL);
    
    if (0 > conn.fd) {
      error("accept failed");
      close(sock_fd);
      exit(EXIT_FAILURE);
    }

    info("conn init");
    
    marten_request_parse(&conn.request, NULL, marten_init, NULL, 0);
    conn.response.write = do_write;

    char buffer[128];
    int res = 0;
    ssize_t readed = 0;
    
    for (; res == 0 && (readed = read(conn.fd, buffer, sizeof(buffer))) > 0; ) {
      rawlog("recv", buffer, readed);
      res = marten_request_parse(&conn.request, on_parse, marten_none, buffer, readed);
    }
    
    for (; 0 == marten_resume_write(&conn.response); );
    
    if (-1 == shutdown(conn.fd, SHUT_RDWR)) {
      error("shutdown failed");
      close(conn.fd);
      close(sock_fd);
      exit(EXIT_FAILURE);
    }
    
    close(conn.fd);
    info("conn done");
  }
  
  close(sock_fd);
  return EXIT_SUCCESS;
}
