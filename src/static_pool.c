#include "common.h"

typedef struct {
  marten_conn_state_t state;
} stateful_conn_t;

#define conn ((stateful_conn_t*)base_conn)

marten_conn_t *marten_conn_init_static(const marten_pool_t *conn_pool) {
  marten_pool_static_t *pool = marten_containerof(conn_pool, marten_pool_static_t, conn_pool);
  
  size_t i = 0;
  for (; i < pool->pool_size; i++) {
    marten_conn_t *base_conn = (uint8_t*)pool->pool_base + i * pool->conn_size;
    if (!marten_has(conn->state, marten_conn_used)) {
      marten_set(conn->state, marten_conn_used);
      return base_conn;
    }
  }
  
  return NULL;
}

void marten_conn_done_static(const marten_pool_t *conn_pool,
                             marten_conn_t *base_conn) {
  (void)conn_pool;
  
  marten_reset(conn->state, marten_conn_used);
}

marten_conn_t *marten_conn_next_static(const marten_pool_t *conn_pool,
                                       const marten_conn_t *base_conn) {
  marten_pool_static_t *pool = marten_containerof(conn_pool, marten_pool_static_t, conn_pool);
  
  if (base_conn == NULL) {
    return pool->pool_base;
  }
  
  const marten_conn_t *end_conn = (const uint8_t*)pool->pool_base + pool->conn_size * pool->pool_size;
  
  for (; (base_conn = (uint8_t*)base_conn + pool->conn_size) < end_conn; ) {
    if (marten_has(conn->state, marten_conn_used)) {
      return (marten_conn_t*)base_conn;
    }
  }
  
  return NULL;
}
