{div, legend, fieldset, label, input, select, option, button, ul, li, a} = require "./node"

empty = {}
dummy = ->

Control = (opts, elem)->
  if opts.nowrap
    elem
  else
    div
      class:
        "pure-control-group": on
      if opts.label
        label
          attrs:
            for: opts.id
          opts.label
      elem

@Entry = (opts)->
  change = if opts.change?
    ({target:{value}})-> opts.change value if value isnt opts.value
  else
    dummy
  
  Control opts,
    input
      attrs:
        id: opts.id
        type: opts.password and "password" or "text"
        value: opts.value
        disabled: opts.disabled
        required: opts.required
        placeholder: opts.placeholder
      props:
        value: opts.value
      on:
        keyup: change
        keydown: opts.keydown
        change: change

@Select = (opts)->
  change = if opts.change?
    ({target:{value}})-> opts.change value
  else
    dummy
  Control opts,
    select
      attrs:
        id: opts.id
        disabled: opts.disabled
        required: opts.required
      on:
        change: change
      for opt in opts.options
        option
          attrs:
            value: opt.value
            selected: opt.value is opts.value
          opt.label

fnChecked = (values)->
  if Array.isArray values
    [
      (value)-> -1 isnt values.indexOf value # is checked
    ,
      (value)-> # trigger checked
        index = values.indexOf value
        if index is -1
          values = [values..., value]
        else
          values = [values[...index]..., values[index+1...]...]
        values
    ]
  else if "object" is typeof values
    [
      (value)-> values[value] # is checked
    ,
      (value)-> # trigger checked
        _values = values
        values = {}
        values[key] = val for key, val of _values
        values[value] = not values[value]
        values
    ]
  else
    [ dummy, dummy ]

@Check = (opts)->
  if opts.options
    pfx = opts.id and opts.id + "-" or ""
    
    [check, trig] = fnChecked opts.value
    
    change = if opts.change?
      (value)-> opts.change trig value
    else
      dummy
    
    Control opts,
      if opts.buttons
        for opt in opts.options
          style = opt.style or opts.style
          button
            class:
              "pure-button": on
              "pure-button-#{style}": style?
              "pure-button-active": check opt.value
              "pure-button-disabled": opts.disabled
            attrs:
              disabled: opts.disabled
            on:
              click: [change, opt.value]
            opt.label
      else
        for opt in opts.options
          id = pfx + opt.value
          label
            class:
              "pure-checkbox": on
              "input-helper": on
              "input-helper-checkbox": on
            attrs:
              for: id
            input
              attrs:
                id: id
                type: "checkbox"
                checked: check opt.value
                disabled: opts.disabled
                required: opts.required
              on:
                change: [change, opt.value]
            " "
            opt.label
  else
    change = if opts.change?
      -> opts.change not opts.value
    else
      dummy
    div
      class:
        "pure-controls": on
      if opts.button
        button
          class:
            "pure-button": on
            "pure-button-#{opts.style}": opts.style?
            "pure-button-active": check opt.value
            "pure-button-disabled": opts.disabled
          attrs:
            disabled: opts.disabled
          on:
            click: change
          opts.label
      else
        label
          class:
            "pure-checkbox": on
          attrs:
            for: opts.id
          input
            attrs:
              id: opts.id
              type: "checkbox"
              checked: opts.value
              disabled: opts.disabled
              required: opts.required
            on:
              change: change
          opts.label

@Radio = (opts)->
  pfx = opts.id and opts.id + "-" or ""
  name = pfx + "radio"
  
  change = opts.change or dummy
  
  Control opts,
    for opt in opts.options
      id = pfx + opt.value
      label
        class:
          "pure-radio": on
        attrs:
          for: id
        input
          class:
            "input-helper": on
            "input-helper-radio": on
          attrs:
            id: id
            name: name
            type: "radio"
            checked: opts.value is opt.value
          on:
            change: [change, opt.value]
        opt.label

@Button = (opts)->
  button
    class:
      "pure-button": on
      "pure-button-#{opts.style}": opts.style?
      "pure-button-active": opts.active
      "pure-button-disabled": opts.disabled
    attrs:
      disabled: opts.disabled
    on:
      click: opts.click or dummy
    opts.label

@Form = (opts, fields...)->
  opts ?= empty
  div
    class:
      "pure-form": on
      "pure-form-aligned": opts.aligned
    fields

@Group = (opts, controls...)->
  opts ?= empty
  fieldset
    class:
      "pure-controls": opts.controls
    if opts?.label
      legend null,
        opts.label
    controls

@Combo = (opts)->
  change = opts.change or dummy
  
  if opts.options?
    for opt in opts.options when opt.value is opts.value
      title = opt.label
      break

  unless title?
    custom = on if opts.custom
    title = opts.value

  if opts.title?
    title = if "function" is typeof opts.title
      opts.title title
    else
      opts.title
  
  Control opts,
    div
      class:
        "pure-menu": on
        "pure-menu-horizontal": on
      ul
        class:
          "pure-menu-list": on
        li
          class:
            "pure-menu-item": on
            "pure-menu-has-children": on
          a
            attrs:
              id: opts.id + "-open"
              href: "#"
            class:
              "pure-menu-link": on
            title
          ul
            class:
              "pure-menu-children": on
            if opts.custom
              custom_change = ({target:{value}})-> change value, yes
              custom_value = custom and opts.value or ""
              li
                class:
                  "pure-menu-item": on
                input
                  class:
                    "pure-menu-link": on
                  attrs:
                    id: opts.id
                    type: "text"
                    value: custom_value
                    disabled: opts.disabled
                    required: opts.required
                    placeholder: opts.placeholder
                  props:
                    value: custom_value
                  on:
                    keyup: custom_change
                    change: custom_change
            if opts.options?
              for opt in opts.options
                li
                  class:
                    "pure-menu-item": on
                  a
                    attrs:
                      href: "#"
                    class:
                      "pure-menu-link": on
                    on:
                      click: [change, opt.value, no]
                    opt.label
