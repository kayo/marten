test.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

test.INHERIT := marten-pp libmarten

test.list ?= request_parser response_parser

define MARTEN_TEST_RULES
TARGET.LIBS += libtest/$(1)
libtest/$(1).INHERIT := test
libtest/$(1).SRCS += $(test.BASEPATH)$(1).c

TARGET.BINS += test/$(1)
test/$(1).INHERIT := test
test/$(1).DEPLIBS* := libmarten libtest/$(1)

test: test.$(1)
test.$(1): run.test/$(1)
endef

$(call ADDRULES,MARTEN_TEST_RULES:test.list)
