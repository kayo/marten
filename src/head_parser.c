#include "common.h"
#include "parser.h"

#define states(state)              \
  state(recv_header_feed_cr)       \
  state(recv_header_feed_lf)       \
  state(recv_header_field_seed)    \
  state(recv_header_field)         \
  state(recv_header_value_seed)    \
  state(recv_header_value)

#define flushes(state, state_range)        \
  state(recv_header_field, marten_field)   \
  state(recv_header_value, marten_value)

enum {
  _marten_recv_done = 0,
#define state(name) _marten_##name,
  states(state)
#undef state
};

/* parser runtime state */
typedef struct {
  marten_handler_t *handler;
  marten_head_parser_t *state;
  const char *ini;
  const char *ptr;
  const char *end;
  const char *tmp;
  int res;
  marten_flags_t ent;
} _marten_spr_t;

static void _marten_emit(_marten_spr_t *sprp) {
  const char *ptr = sprp->tmp ? sprp->tmp : sprp->ini;
  size_t len = sprp->ptr - ptr;
  marten_flags_t flags = marten_none;
  
  switch (*sprp->state) {
#define state(name, _flags) case _marten_##name: flags = _flags; break;
#define state_range(name1, name2, _flags) case _marten_##name1 ... _marten_##name2: flags = _flags; break;
    flushes(state, state_range);
#undef state
#undef state_range
  }
  
  if (flags) {
    flags |= sprp->ent;
    sprp->res = sprp->handler(sprp->state,
                              (sprp->tmp ? marten_init : 0) |
                              (sprp->ptr == sprp->end ? 0 : marten_done) |
                              flags, ptr, len);
  }
  
  if (sprp->tmp) {
    sprp->tmp = NULL;
  }
}

chunk(recv_header_feed_cr) {
  if (likely(ptr_char == '\r')) {
    turn(recv_header_feed_lf);
  } else {
    fail();
  }
}

chunk(recv_header_feed_lf) {
  if (likely(ptr_char == '\n')) {
    turn(recv_header_field_seed);
  } else {
    fail();
  }
}

chunk(recv_header_field_seed) {
  if (likely(istok(ptr_char))) {
    turn(recv_header_field);
    ent_init();
  } else if (likely(issep(ptr_char))) {
    turn(recv_header_value_seed);
  } else {
    fail();
  }
}

chunk(recv_header_field) {
  if (likely(istok(ptr_char))) {
    ent_char();
  } else if (likely(ptr_char == ':')) {
    ent_done();
    turn(recv_header_value_seed);
  } else {
    fail();
  }
}

chunk(recv_header_value_seed) {
  if (unlikely(!issep(ptr_char))) {
    turn(recv_header_value);
    ent_init();
  }
}

chunk(recv_header_value) {
  if (unlikely(ptr_char == '\r')) {
    ent_done();
    turn(recv_header_feed_lf);
  } else {
    ent_char();
  }
}

int marten_head_parse(marten_request_parser_t *state,
                      marten_handler_t *handler,
                      marten_flags_t flags,
                      const char *ptr, size_t len) {
  if (marten_has(flags, marten_init)) {
    *state = _marten_recv_header_field_seed;
  }
  
  if (ptr == NULL) {
    return 0;
  }
  
  _marten_spr_t spr = {
    handler,
    state,
    ptr,
    ptr,
    ptr + len,
    NULL,
    0,
    flags & marten_entity
  };
  
  for (; spr.ptr < spr.end && *state != _marten_recv_done && spr.res == 0; spr.ptr++) {
    switch (*state) {
#define state(name) case _marten_##name: _marten_##name##_chunk(&spr); break;
      states(state);
#undef state
    default: spr.res = 1;
    }
  }
  
  if (spr.res == 0) {
    if (marten_has(flags, marten_done)) {
      spr.end++;
    }
    
    _marten_emit(&spr);
    
    if (marten_has(flags, marten_done)) {
      *state = _marten_recv_done;
    }
  }
  
  return spr.res;
}
